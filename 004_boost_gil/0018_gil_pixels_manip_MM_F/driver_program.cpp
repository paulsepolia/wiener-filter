//=====================//
// image I/O with GIL  //
// pixels manipulation //
//=====================//

// C++

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;
using std::to_string;

// BOOST/GIL

#include <boost/mpl/vector.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/extension/dynamic_image/any_image.hpp>
#include <boost/gil/image_view.hpp>
#include <boost/gil/planar_pixel_reference.hpp>
#include <boost/gil/color_convert.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/image_view_factory.hpp>
#ifndef BOOST_GIL_NO_IO
#include <boost/gil/extension/io/tiff_io.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <boost/gil/extension/io/tiff_dynamic_io.hpp>
#include <boost/gil/extension/io/jpeg_dynamic_io.hpp>
#include <boost/gil/extension/io/png_dynamic_io.hpp>
#endif

using namespace boost::gil;

// the main function

int main() 
{
	// local parameters

	const string in_dir = "../images_in/";
     const string out_dir = "../images_out/";
	const unsigned int MAX_LEN = 5;
	const string str_zero = "00000";
	const unsigned int K_MAX = 1000;
     const string end_name_out = "_image.jpg";

	// local variables

	string im_name;
	gray8_image_t my_im;
	string tmp_str1;
	string tmp_str2;

	// image dimensions

	const int w = 1000;
	const int h = w;
	const size_t sizeImage = w * h;
	vector<unsigned char> bitmap;

	// fill the bitmaps with some data

	for(unsigned int k = 0; k != K_MAX; ++k)
	{
		for (unsigned int i = 0; i != sizeImage; ++i)
		{
			bitmap.push_back((k+255)%255);
		}
		
		tmp_str1 = to_string(k);
          tmp_str2 = str_zero;
          tmp_str2.append(tmp_str1);

         	if (tmp_str2.length() > MAX_LEN)
         	{
         		reverse(tmp_str2.begin(), tmp_str2.end());
              	tmp_str2.resize(MAX_LEN);
              	reverse(tmp_str2.begin(), tmp_str2.end());
         	}

		auto im_new = interleaved_view(w, h, (gray8_pixel_t const*)(&bitmap[0]), w);

		const string image_file_out = out_dir + tmp_str2 + end_name_out;

		jpeg_write_view(image_file_out, im_new);

		bitmap.clear();
		bitmap.shrink_to_fit();
	}

	// return

	return 0;
}

// FINI
