
//====================//
// image manipulation //
//====================//

#include <iostream>
#include <string>
#include <cmath> 

#include <boost/mpl/vector.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/extension/dynamic_image/any_image.hpp>
#include <boost/gil/image_view.hpp>
#include <boost/gil/planar_pixel_reference.hpp>
#include <boost/gil/color_convert.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/image_view_factory.hpp>
#ifndef BOOST_GIL_NO_IO
#include <boost/gil/extension/io/tiff_io.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <boost/gil/extension/io/tiff_dynamic_io.hpp>
#include <boost/gil/extension/io/jpeg_dynamic_io.hpp>
#include <boost/gil/extension/io/png_dynamic_io.hpp>
#endif

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::pow;

using namespace boost::gil;

typedef any_image<boost::mpl::vector<gray8_image_t, bgr8_image_t, argb8_image_t,
                                     rgb8_image_t, rgb8_planar_image_t, 
                                     cmyk8_image_t, cmyk8_planar_image_t, 
                                     rgba8_image_t, rgba8_planar_image_t>> any_image_t;

#ifdef BOOST_GIL_NO_IO

void test_image_io() 
{ 
 	cout <<  " --> IO is not tested when BOOST_GIL_NO_IO is enabled" << endl;
}

#else 

void test_image_io() 
{
	const string in_dir = "";  // directory of source images
    	const string out_dir = in_dir + "image_io_out//";

	//
	// --> GRAY IMAGE
	//

    	gray8_image_t imgGray;

	// --> TIFF

   	// load gray tiff into gray image

	cout << " -->  1 --> load --> gray.tif" << endl;
    	
	tiff_read_image(in_dir+"gray.tif", imgGray);

    	// save gray image to tiff

	cout << " -->  2 --> save --> grayFromGray.tif" << endl;
	
	tiff_write_view(out_dir+"grayFromGray.tif", view(imgGray));

    	// load RGB tiff into gray image

	cout << " -->  3 --> load --> RGB.tif" << endl;

	tiff_read_and_convert_image(in_dir+"RGB.tif", imgGray);

    	// save gray image to tiff (again)
    
	cout << " -->  4 --> save --> grayFromRGB.tif" << endl;

	tiff_write_view(out_dir+"grayFromRGB.tif", view(imgGray));

	// --> JPEG

    	// load gray jpeg into gray image

	cout << " -->  5 --> load --> gray.jpg" << endl;
 
	jpeg_read_image(in_dir+"gray.jpg", imgGray);
    
	// save gray image to gray jpeg
    
	cout << " -->  6 --> save --> grayFromGray.jpg" << endl;

	jpeg_write_view(out_dir+"grayFromGray.jpg", view(imgGray));
    
    	// load RGB jpeg into gray image
 
	cout << " -->  7 --> load --> RGB.jpg" << endl;

	jpeg_read_and_convert_image(in_dir+"RGB.jpg", imgGray);
    	
	// save gray image to RGB jpeg

	cout << " -->  8 --> save --> grayFromRGB.jpg" << endl;

	jpeg_write_view(out_dir+"grayFromRGB.jpg", color_converted_view<rgb8_pixel_t>(view(imgGray)));

	// --> PNG

	// load gray png into gray image

	cout << " -->  9 --> load --> gray.png" << endl;
   
	png_read_image(in_dir+"gray.png", imgGray);
    
	// save gray image to gray png

	cout << " --> 10 --> save --> grayFromGray.png" << endl;

	png_write_view(out_dir+"grayFromGray.png", view(imgGray));
    
	// load RGB png into gray image
    
	cout << " --> 11 --> load --> RGB.png --> NO" << endl;

	//png_read_and_convert_image(in_dir+"RGB.png", imgGray);
    
	// save gray image to RGB png
 
	cout << " --> 12 --> save --> grayFromRGB.png --> NO" << endl;

	//png_write_view(out_dir+"grayFromRGB.png", color_converted_view<rgb8_pixel_t>(view(imgGray)));

	//
	// --> RGB Planar
	//

    	rgb8_image_t imgRGB;

	// TIFF

    	// load gray tiff into RGB image

	cout << " --> 13 --> load --> gray.tif" << endl;
 
	tiff_read_and_convert_image(in_dir+"gray.tif", imgRGB);
    
	// save RGB image to tiff

	cout << " --> 14 --> save --> RGBFromGray.tif --> NO" << endl;

	//tiff_write_view(out_dir+"RGBFromGray.tif", view(imgRGB));

    	// load RGB tiff into RGB image

	cout << " --> 15 --> load --> RGB.tif --> NO" << endl;
 
	//tiff_read_image(in_dir+"RGB.tif", imgRGB);
    
	// save RGB image to tiff (again!)
    
	cout << " --> 16 --> save --> RGBFromRGB.tif" << endl;

	tiff_write_view(out_dir+"RGBFromRGB.tif", view(imgRGB));

	// JPEG
    
	// load gray jpeg into RGB image

	cout << " --> 17 --> load --> gray.jpg" << endl;

	jpeg_read_and_convert_image(in_dir+"gray.jpg", imgRGB);
    
	// save RGB image to gray jpeg

	cout << " --> 18 --> save --> RGBFromGray.jpg" << endl;

	jpeg_write_view(out_dir+"RGBFromGray.jpg", view(imgRGB));
    
    	// load RGB jpeg into RGB image

	cout << " --> 19 --> load --> RGB.jpg" << endl;

	jpeg_read_image(in_dir+"RGB.jpg", imgRGB);
    
	// save RGB image to RGB jpeg

	cout << " --> 20 --> save --> RGBFromRGB.jpg" << endl;

	jpeg_write_view(out_dir+"RGBFromRGB.jpg", view(imgRGB));

	// PNG
    
	// load gray png into RGB image

	cout << " --> 21 --> load --> gray.png" << endl;
 
	png_read_and_convert_image(in_dir+"gray.png", imgRGB);
    
	// save RGB image to gray png

	cout << " --> 22 --> save --> RGBFromGray.png" << endl;

	png_write_view(out_dir+"RGBFromGray.png", view(imgRGB));
    
    	// load RGB png into RGB image

	cout << " --> 23 --> load --> RGB.png --> NO" << endl;

	//png_read_image(in_dir+"RGB.png",imgRGB);
    
	// save RGB image to RGB png
 
	cout << " --> 24 --> save --> RGBFromRGB.png --> NO" << endl;

	//png_write_view(out_dir+"RGBFromRGB.png", view(imgRGB));

	//	
	// --> GRAY32 Planar
	//    

	gray32f_image_t imgGray32;

	// TIFF

    	// load gray tiff into gray image

	cout << " --> 25 --> load --> gray.tif" << endl;

	tiff_read_and_convert_image(in_dir+"gray.tif", imgGray32);
    
	// save gray image to tiff

	cout << " --> 26 --> save --> gray32FromGray.tif" << endl;

	tiff_write_view(out_dir+"gray32FromGray.tif", view(imgGray32));
        
    	// load RGB tiff into gray image

	cout << " --> 27 --> load --> RGB.tif" << endl;

	tiff_read_and_convert_image(in_dir+"RGB.tif", imgGray32);

    	// save gray image to tiff (again!)

	cout << " --> 28 --> save --> gray32FromRGB.tif" << endl;

	tiff_write_view(out_dir+"gray32FromRGB.tif", view(imgGray32));

	// JPEG

	cout << " --> 29 --> load --> gray.tif" << endl;

	tiff_read_and_convert_image(in_dir+"gray.tif", imgGray32); // again TIF (jpeg load not supported)
    
	// save RGB image to gray jpeg

	cout << " --> 30 --> save --> gray32FromGray.jpg" << endl;

	tiff_write_view(out_dir+"gray32FromGray.jpg", view(imgGray32));
    
    	// load RGB jpeg into RGB image
    
	cout << " --> 31 --> load --> RGB.tif" << endl;

	tiff_read_and_convert_image(in_dir+"RGB.tif", imgGray32);  // again TIF (jpeg load not supported)
    
	// save RGB image to RGB jpeg

	cout << " --> 32 --> save --> gray32FromRGB.jpg" << endl;

	tiff_write_view(out_dir+"gray32FromRGB.jpg", color_converted_view<rgb32f_pixel_t>(view(imgGray32)));

	//
	// --> NATIVE Planar
	//    

	any_image_t anyImg;

	// TIFF
    
	// load RGB tiff into any image
    
	cout << " --> 33 --> load --> RGB.tif --> NO" << endl;

	//tiff_read_image(in_dir+"RGB.tif", anyImg);

    	// save any image to tiff
    
	cout << " --> 34 --> save --> RGBNative.tif --> NO" << endl;

	//tiff_write_view(out_dir+"RGBNative.tif", view(anyImg));

    	// load gray tiff into any image
    
	cout << " --> 35 --> load --> gray.tif" << endl;

	tiff_read_image(in_dir+"gray.tif", anyImg);
    
    	// save any image to tiff
    
	cout << " --> 36 --> save --> grayNative.tif" << endl;

	tiff_write_view(out_dir+"grayNative.tif", view(anyImg));

	// JPEG
    
	// load gray jpeg into any image
    
	cout << " --> 37 --> load --> gray.jpg" << endl;

	jpeg_read_image(in_dir+"gray.jpg", anyImg);
    
	// save any image to jpeg
    
	cout << " --> 38 --> save --> grayNative.jpg" << endl;

	jpeg_write_view(out_dir+"grayNative.jpg", view(anyImg));
    
    	// load RGB jpeg into any image
    
	cout << " --> 39 --> load --> RGB.jpg" << endl;

	jpeg_read_image(in_dir+"RGB.jpg", anyImg);
    
	// save any image to jpeg
    
	cout << " --> 40 --> save --> RGBNative.jpg" << endl;

	jpeg_write_view(out_dir+"RGBNative.jpg", view(anyImg));

	// PNG
    
	// load gray png into any image
    
	cout << " --> 41 --> load --> gray.png" << endl;

	png_read_image(in_dir+"gray.png", anyImg);
    
	// save any image to png
    
	cout << " --> 42 --> save --> grayNative.png" << endl;

	png_write_view(out_dir+"grayNative.png", view(anyImg));
    
    	// load RGB png into any image
    
	cout << " --> 43 --> load --> RGB.png --> NO" << endl;

	//png_read_image(in_dir+"RGB.png", anyImg);
    
	// save any image to png
    
	cout << " --> 44 --> save --> RGBNative.png --> NO" << endl;

	//png_write_view(out_dir+"RGBNative.png", view(anyImg));

}

#endif

// the main function

int main(int argc, char* argv[]) 
{
	const int I_MAX = static_cast<int>(pow(10.0, 5.0));

	// call the test function

	for (int i = 0; i < I_MAX; i++)
	{
		cout << "----------------------------------------------->> " << i << endl;

		test_image_io();
	}

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
	
	return 0;
}

//======//
// FINI //
//======//

