
#ifndef MATRIX_DENSE_MEMORY_H
#define MATRIX_DENSE_MEMORY_H

namespace pgg {

// Allocate

template <typename T1>
void MatrixDense<T1>::Allocate(const unsigned & num_rows,
                               const unsigned & num_cols)
{
     // delete heap space if
     // has been allocated already

     if (this->mat_ptr != 0) {
          this->Deallocate();
     } else if (this->mat_ptr == 0) {}

     // set the dimensions

     this->rows = num_rows;
     this->cols = num_cols;

     // allocate heap space

     const auto ROWS = static_cast<unsigned long>(this->rows);
     const auto COLS = static_cast<unsigned long>(this->cols);

     this->mat_ptr = new T1[ROWS * COLS];
}

// Deallocate

template <typename T1>
void MatrixDense<T1>::Deallocate()
{
     // delete heap space

     delete[] this->mat_ptr;

     // set to null pointer

     this->mat_ptr = 0;
}

} // pgg

#endif // MATRIX_DENSE_MEMORY_H

