#!/bin/bash

  # 1. compile

  g++  -O3                 \
       -Wall               \
       -std=c++0x          \
       wienerFilterY2D.cpp \
       driver_program.cpp  \
       -ljpeg              \
       -ltiff              \
       -lpng               \
       -lz                 \
       -o x_gnu
