//=====================//
// image I/O with GIL  //
// pixels manipulation //
//=====================//

// C++

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::vector;

// BOOST/GIL

#include <boost/mpl/vector.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/extension/dynamic_image/any_image.hpp>
#include <boost/gil/image_view.hpp>
#include <boost/gil/planar_pixel_reference.hpp>
#include <boost/gil/color_convert.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/image_view_factory.hpp>
#ifndef BOOST_GIL_NO_IO
#include <boost/gil/extension/io/tiff_io.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <boost/gil/extension/io/tiff_dynamic_io.hpp>
#include <boost/gil/extension/io/jpeg_dynamic_io.hpp>
#include <boost/gil/extension/io/png_dynamic_io.hpp>
#endif

using namespace boost::gil;

// the main function

int main() 
{
	// local parameters

	const string in_dir = "../images_in/";
     const string out_dir = "../images_out/";
	const string im_in = "gray.jpg";
	const string im_out = "gray_out.jpg";
	const string im_outA = "gray_outA.jpg";
	const double ZERO = 0.0;
	const double T55 = 255.0;

	// local variables

	gray8_image_t imA;
	unsigned int i;
	unsigned int j;
	vector<unsigned char> vecA;
	vector<double> vecB;

     // load jpeg image

     jpeg_read_image(in_dir + im_in, imA);

     // save image to jpeg

     jpeg_write_view(out_dir + im_out, view(imA));

	// get dimensions of the image

	const unsigned int H = imA.height();
	const unsigned int W = imA.width();
	const unsigned long int imSize = static_cast<unsigned long int>(W) * H;

	// put the image in vectors
	
	for (i = 0; i != H; ++i)
	{
		for (j = 0; j != W; ++j)
		{
			auto iL = i*static_cast<unsigned long int>(W)+j;

			vecA.push_back(static_cast<unsigned char>(view(imA)[iL]));
			vecB.push_back(static_cast<double>(view(imA)[iL]));
		}
	}

	// declare image container

	auto imOutA = interleaved_view(W, H, (gray8_pixel_t const*)(&vecA[0]), W);

	// write the new image

	jpeg_write_view(out_dir+im_outA, imOutA);

	// test --> 1

	for (unsigned long int iL = 0; iL < imSize; ++iL)
	{
	 	if(vecB[iL] < ZERO)
		{
			cout << "ERROR --> 1 --> " << iL << endl;
		}

	 	if(vecB[iL] > T55)
		{
			cout << "ERROR --> 2 --> " << iL << endl;
		}
	}

	// test --> 2

	cout << vecB[0] << endl;
	cout << vecB[1] << endl;
	cout << vecB[2] << endl;
	cout << vecB[3] << endl;

	// return

	return 0;
}

// FINI
