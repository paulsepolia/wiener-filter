#!/bin/bash

  # 1. compile

  g++  -O3                 \
       -Wall               \
       -std=c++0x          \
	  -static             \
       driver_program.cpp  \
	  -ljpeg 			  \
	  -ltiff 			  \
	  -lpng  			  \
	  -lz     		  \
       -o x_gnu


