
//===============//
// rescale image //
//===============//

#include <iostream>

#include <boost/gil/image.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/numeric/sampler.hpp>
#include <boost/gil/extension/numeric/resample.hpp>

using std::cout;
using std::cin;
using std::endl;

using namespace boost::gil;

// the main function

int main() 
{
	rgb8_image_t img;

    	jpeg_read_image("RGB.jpg", img);

    	// test resize_view
    	// Scale the image to 10000x10000 pixels using bilinear resampling

    	rgb8_image_t square100x100(10000,10000);

    	resize_view(const_view(img), view(square100x100), bilinear_sampler());

    	jpeg_write_view("out-resize.jpg", const_view(square100x100));

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
	
    	return 0;
}

//======//
// FINI //
//======//

