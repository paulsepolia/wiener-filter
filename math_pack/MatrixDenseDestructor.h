
#ifndef MATRIX_DENSE_DESTRUCTOR_H
#define MATRIX_DENSE_DESTRUCTOR_H

namespace pgg {

// destructor

template<typename T1>
MatrixDense<T1>::~MatrixDense()
{
     if (this->mat_ptr != 0) {
          this->Deallocate();
     } else if (this->mat_ptr == 0) {
     }
}

} // pgg

#endif // MATRIX_DENSE_DESTRUCTOR_H

