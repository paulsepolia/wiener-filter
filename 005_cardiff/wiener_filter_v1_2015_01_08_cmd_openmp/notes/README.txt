
 1. Wiener Filter - Version 1 - OpenMP - Command Line version

    Uses OpenCV only to read the input image from the hard disk
    and then to save the modified image to the hard disk.
    The input image can be of any type and size.
    The output image is a grayscale image of the original size.
    The manipulation of image is done via representing as
    a matrix of doubles in range (0.0, 255.0).
    The value range is mandatory, 
    so any other value range, does not work.

 2. Wiener Filter - Version 2 - NOT READY YET

    Uses GIL/BOOST library for any I/O image operations.
    So OpenCV has been removed completely from version 2.
    The rest functionality is exact the same as in version 1.
