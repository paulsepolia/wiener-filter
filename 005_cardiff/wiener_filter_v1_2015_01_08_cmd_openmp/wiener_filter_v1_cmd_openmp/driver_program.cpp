//========//
// Driver //
//========//

// C++

#include <iostream>
#include <string>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::string;

using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// OpenCV

#include "cv.h"
#include "highgui.h"

using namespace cv;

// pgg

#include "wienerFilterGrayscale2D.h"
#include "../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// the main function

int main(int argc, char ** argv)
{
     // input variables

     string image_name = argv[1];
     string result_name = argv[2];
     string diff_name = argv[3];
     string wdim = argv[4];

     const unsigned int wiener_kernel_rows = atoi(wdim.c_str()); // >= 2
     const unsigned int wiener_kernel_cols = atoi(wdim.c_str());  // >= 2

     // local parameters and variables

     const string image_path = image_name;
     const string result_path = result_name;
     const string diff_path = diff_name;
     MatrixDense<double> imageMatrixIn;
     MatrixDense<double> imageMatrixOut;
     int ROWS;
     int COLS;

     // opencv variables

     CvMat matTmp1;
     CvMat *matTmp2 = NULL;

     // load image - opencv

     IplImage *im1 = cvLoadImage(image_path.c_str());

     // create image - opencv

     IplImage *im2 = cvCreateImage(cvSize(im1->width, im1->height), IPL_DEPTH_8U, 1);

     // convert to grayscale - opencv

     cvCvtColor(im1, im2, CV_RGB2GRAY);

     // put image to matrix - opencv

     matTmp2 = (CvMat*) im2;
     matTmp2 = cvGetMat(matTmp2, &matTmp1, 0, 1);

     // get the rows and cols of the image - opencv

     ROWS = matTmp2->rows;
     COLS = matTmp2->cols;

     // allocate image matrix

     imageMatrixIn.Allocate(ROWS, COLS);
     imageMatrixOut.Allocate(ROWS, COLS);

     // build image matrix - opencv and mine

     Mat matimg(im2);

     for (int i = 0; i != ROWS; ++i) {
          for (int j = 0; j != COLS; ++j) {
               unsigned char tmp;
               tmp = ((uchar*) matimg.data + i * matimg.step)[j * matimg.elemSize() + 1];
               imageMatrixIn.SetElement(i, j, tmp);
          }
     }

     // apply the wiener filter

     wienerFilterGrayscale2D(imageMatrixIn,
                             imageMatrixOut,
                             wiener_kernel_rows,
                             wiener_kernel_cols);

     // opencv way to save image

     IplImage * imOut;
     IplImage * imDiff;

     CvSize imOutSize;
     CvSize imDiffSize;

     imOutSize.width = COLS;
     imOutSize.height = ROWS;

     imDiffSize.width = COLS;
     imDiffSize.height = ROWS;

     imOut = cvCreateImage(imOutSize, 8, 1);
     imDiff = cvCreateImage(imDiffSize, 8, 1);

     // output image

     for (int i = 0; i != ROWS; ++i) {
          for (int j = 0; j != COLS; ++j) {
               ((uchar*)(imOut->imageData + imOut->widthStep*i))[j] =
                    static_cast<char>(imageMatrixOut(i,j));
          }
     }

     // diff image

     for (int i = 0; i != ROWS; ++i) {
          for (int j = 0; j != COLS; ++j) {
               ((uchar*)(imDiff->imageData + imDiff->widthStep*i))[j] =
                    static_cast<char>(fabs(imageMatrixOut(i,j)-imageMatrixIn(i,j)));
          }
     }

     // save images - opencv

     cvSaveImage(result_path.c_str(), imOut);
     cvSaveImage(diff_path.c_str(), imDiff);

     // free up RAM - opencv

     cvReleaseImage(&im1);
     cvReleaseImage(&im2);
     cvReleaseImage(&imOut);
     cvReleaseImage(&imDiff);

     // deallocate matrices

     imageMatrixIn.Deallocate();
     imageMatrixOut.Deallocate();

     // return

     return 0;
}

// END
