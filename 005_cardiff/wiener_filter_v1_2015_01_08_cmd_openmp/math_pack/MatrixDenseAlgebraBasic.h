
#ifndef MATRIX_DENSE_ALGEBRA_BASIC_H
#define MATRIX_DENSE_ALGEBRA_BASIC_H

typedef unsigned int MAI;
typedef unsigned long int MAI_MAX;

namespace pgg {

// Divide

template<typename T1>
void MatrixDense<T1>::Divide(const MatrixDense<T1> & matA,
                             const MatrixDense<T1> & matB)
{
     // test for matrices compatibility

     this->CheckCompatibility(matA);
     this->CheckCompatibility(matB);

     // main code

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI COLS = matA.cols;
     const MAI ROWS = matA.rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(matA)     \
     shared(matB)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = matA.mat_ptr[index] /
                              matB.mat_ptr[index];
                    this->mat_ptr[index] = tmp_val;
               }
          }
     }
}

// Plus # 1

template<typename T1>
void MatrixDense<T1>::Plus(const MatrixDense<T1> & matA,
                           const MatrixDense<T1> & matB)
{
     // test for matrices compatibility

     this->CheckCompatibility(matA);
     this->CheckCompatibility(matB);

     // main code

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(matA)     \
     shared(matB)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = matA.mat_ptr[index] + matB.mat_ptr[index];
                    this->mat_ptr[index] = tmp_val;
               }
          }
     }
}

// Plus # 2

template<typename T1>
void MatrixDense<T1>::Plus(const MatrixDense<T1> & matA,
                           const T1 & elem)
{
     // test for matrices compatibility

     this->CheckCompatibility(matA);

     // main code

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(matA)     \
     shared(elem)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = matA.mat_ptr[index] + elem;
                    this->mat_ptr[index] = tmp_val;
               }
          }
     }
}

// Subtract

template<typename T1>
void MatrixDense<T1>::Subtract(const MatrixDense<T1> & matA,
                               const MatrixDense<T1> & matB)
{
     this->Plus(matA, -matB);
}


// Times # 1

template<typename T1>
void MatrixDense<T1>::Times(const MatrixDense<T1> & matA,
                            const MatrixDense<T1> & matB)
{
     // test for matrices compatibility

     this->CheckCompatibility(matA);
     this->CheckCompatibility(matB);

     // main code

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI COLS = matA.cols;
     const MAI ROWS = matA.rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(matA)     \
     shared(matB)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = matA.mat_ptr[index] * matB.mat_ptr[index];
                    this->mat_ptr[index] = tmp_val;
               }
          }
     }
}

// Times # 2

template<typename T1>
void MatrixDense<T1>::Times(const MatrixDense<T1> & matA,
                            const T1 & elem)
{
     // test for matrices compatibility

     this->CheckCompatibility(matA);

     // main code

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI COLS = matA.cols;
     const MAI ROWS = matA.rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(matA)     \
     shared(elem)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = matA.mat_ptr[index] * elem;
                    this->mat_ptr[index] = tmp_val;
               }
          }
     }
}

} // pgg

#endif // MATRIX_DENSE_ALGEBRA_BASIC_H

