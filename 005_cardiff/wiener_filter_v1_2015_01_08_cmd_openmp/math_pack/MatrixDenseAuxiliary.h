
#ifndef MATRIX_DENSE_AUXILIARY_H
#define MATRIX_DENSE_AUXILIARY_H

#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <utility>

namespace pgg {

// CheckCompatibility

template<typename T1>
void MatrixDense<T1>::CheckCompatibility(
     const MatrixDense<T1> & matB) const
{

#include <iostream>

     // test for columns equality

     if (this->cols != matB.cols) {
          std::cout << "ERROR: dense matrices do not have equal number of columns"
                    << std::endl;
          std::cout << "Enter an integer to exit: ";
          int sentinel;
          std::cin >> sentinel;
          exit(-1);
     }

     // test for rows equality

     if (this->rows != matB.rows) {
          std::cout << "ERROR: dense matrices do not have equal number of rows"
                    << std::endl;
          std::cout << "Enter an integer to exit: ";
          int sentinel;
          std::cin >> sentinel;
          exit(-1);
     }
}

// MatrixCorrelate # 1

template <typename T1>
void MatrixDense<T1>::MatrixCorrelate(const MatrixDense<T1> & mat,
                                      const MatrixDense<T1> & kernel,
                                      std::pair<int,int> & anchor)
{
     // check compatibility

     this->CheckCompatibility(mat);

     // get kernel matrix dimensions

     const auto K_ROWS = kernel.GetNumberOfRows();
     const auto K_COLS = kernel.GetNumberOfColumns();

     // get input matrix dimensions

     const auto ROWS = mat.GetNumberOfRows();
     const auto COLS = mat.GetNumberOfColumns();
     const auto COLS_LONG = static_cast<unsigned long>(COLS);

     // local variables and parameters

     unsigned int i;
     unsigned int j;
     unsigned int i_ex;
     unsigned int j_ex;
     unsigned long index;
     unsigned int i_index = 0;
     unsigned int j_index = 0;

     T1 elemDest = static_cast<T1>(0.0);

     #pragma omp parallel default(none) \
     private(i_ex)    \
     private(j_ex)    \
     private(i)       \
     private(j)       \
     private(index)   \
     private(i_index) \
     private(j_index) \
     shared(kernel)  \
     shared(mat)     \
     shared(anchor)  \
     private(elemDest)
     {
          #pragma omp for

          for(i_ex = 0; i_ex < ROWS; ++i_ex) {
               for (j_ex = 0; j_ex < COLS; ++j_ex) {
                    elemDest = static_cast<T1>(0.0);

                    for (i = 0; i < K_ROWS; ++i) {
                         for (j = 0; j < K_COLS; ++j) {

                              i_index = i_ex + i - anchor.first;
                              j_index = j_ex + j - anchor.second;


                              if((i_ex + i - anchor.first) < 0) {
                                   i_index = 0;
                              }

                              if((i_ex + i - anchor.first) >= ROWS) {
                                   i_index = ROWS-1;
                              }

                              if((j_ex + j - anchor.second) < 0) {
                                   j_index = 0;
                              }

                              if((j_ex + j - anchor.second) >= COLS) {
                                   j_index = COLS-1;
                              }

                              elemDest = elemDest + kernel(i,j)*mat(i_index, j_index);
                         }
                    }

                    index = i_ex * COLS_LONG + j_ex;
                    this->mat_ptr[index] = elemDest;
               }
          }
     }
}

// MatrixCorrelate # 2

template <typename T1>
void MatrixDense<T1>::MatrixCorrelate(const MatrixDense<T1> & kernel,
                                      std::pair<int,int> & anchor)
{
     // get kernel matrix dimensions

     const auto K_ROWS = kernel.GetNumberOfRows();
     const auto K_COLS = kernel.GetNumberOfColumns();

     // get input matrix dimensions

     const auto ROWS = this->GetNumberOfRows();
     const auto COLS = this->GetNumberOfColumns();
     const auto COLS_LONG = static_cast<unsigned long>(COLS);

     // local variables and parameters

     unsigned int i;
     unsigned int j;
     unsigned int i_ex;
     unsigned int j_ex;
     unsigned long index;
     unsigned int i_index = 0;
     unsigned int j_index = 0;

     T1 elemDest = static_cast<T1>(0.0);

     #pragma omp parallel default(none) \
     private(i_ex)   \
     private(j_ex)   \
     private(i)      \
     private(j)      \
     shared(index)   \
     shared(i_index) \
     shared(j_index) \
     shared(kernel)  \
     shared(anchor)  \
reduction(+:elemDest)
     {
          #pragma omp for

          for(i_ex = 0; i_ex < ROWS; ++i_ex) {
               for (j_ex = 0; j_ex < COLS; ++j_ex) {
                    elemDest = static_cast<T1>(0.0);

                    for (i = 0; i < K_ROWS; ++i) {
                         for (j = 0; j < K_COLS; ++j) {
                              if((i_ex + i - anchor.first) < 0) {
                                   i_index = 0;
                              }

                              if((i_ex + i - anchor.first) >= ROWS) {
                                   i_index = ROWS-1;
                              }

                              if((j_ex + j - anchor.second) < 0) {
                                   j_index = 0;
                              }

                              if((j_ex + j - anchor.second) >= COLS) {
                                   j_index = COLS-1;
                              }

                              elemDest = elemDest + kernel(i,j)*(*this)(i_index, j_index);
                         }
                    }

                    index = i_ex * COLS_LONG + j_ex;
                    this->mat_ptr[index] = elemDest;
               }
          }
     }
}

// Mean

template<typename T1>
T1 MatrixDense<T1>::Mean() const
{
     unsigned int i;
     unsigned int j;
     T1 mean_val = static_cast<T1>(0.0);

     const auto COLS = this->cols;
     const auto ROWS = this->rows;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);
     const auto DIM_TOT = COLS_LONG * ROWS;

     unsigned long index;

     #pragma omp parallel default(none) \
     private(i)    \
     private(j)    \
     shared(index) \
reduction(+:mean_val)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {
                    index = i * COLS_LONG + j;
                    mean_val = mean_val + this->mat_ptr[index];
               }
          }
     }

     // take the mean

     mean_val = mean_val/static_cast<T1>(DIM_TOT);

     // return

     return mean_val;
}

// SetMax # 1

template<typename T1>
void MatrixDense<T1>::SetMax(const T1 & max_val, const T1 & set_max)
{
     unsigned int i;
     unsigned int j;

     const auto ROWS = this->rows;
     const auto COLS = this->cols;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);
     unsigned long index;

     // build the matrix

     #pragma omp parallel default(none)   \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(max_val)  \
     shared(set_max)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;

                    if (this->mat_ptr[index] > max_val) {
                         this->mat_ptr[index] = static_cast<T1>(set_max);
                    }
               }
          }
     }
}

// SetMax # 2

template<typename T1>
void MatrixDense<T1>::SetMax(const MatrixDense<T1> & mat, const T1 & comp_val)
{
     this->CheckCompatibility(mat);

     unsigned int i;
     unsigned int j;

     const auto ROWS = this->rows;
     const auto COLS = this->cols;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);
     unsigned long index;

     // build the matrix

     #pragma omp parallel default(none)   \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(comp_val) \
     shared(mat)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->mat_ptr[index] = std::max(mat.mat_ptr[index], comp_val);
               }
          }
     }
}

// SetMin

template<typename T1>
void MatrixDense<T1>::SetMin(const T1 & min_val, const T1 & set_min)
{
     unsigned int i;
     unsigned int j;

     const auto ROWS = this->rows;
     const auto COLS = this->cols;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);
     unsigned long index;

     // build the matrix

     #pragma omp parallel default(none)   \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(min_val)  \
     shared(set_min)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;

                    if (this->mat_ptr[index] < min_val) {
                         this->mat_ptr[index] = static_cast<T1>(set_min);
                    }
               }
          }
     }
}

} // pgg

#endif // MATRIX_DENSE_AUXILIARY_H

