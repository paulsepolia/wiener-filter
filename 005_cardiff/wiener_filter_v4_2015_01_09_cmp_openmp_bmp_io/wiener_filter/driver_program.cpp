//========//
// Driver //
//========//

// C++

#include <iostream>
#include <string>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::string;

using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// pgg

#include "../bmp_io/bmpIO.h"
#include "wienerFilterGrayscale2D.h"
#include "../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// the main function

int main(int argc, char ** argv)
{
     // input variables

     string image_name = argv[1];
     string result_name = argv[2];
     string diff_name = argv[3];
     string wdim = argv[4];

     const unsigned int wiener_kernel_rows = atoi(wdim.c_str()); // >= 2
     const unsigned int wiener_kernel_cols = atoi(wdim.c_str());  // >= 2

     // local parameters and variables

     const string image_path = image_name;
     const string result_path = result_name;
     const string diff_path = diff_name;
     MatrixDense<double> imageMatrixIn;
     MatrixDense<double> imageMatrixOut;

     const double rFac = 0.299;
     const double gFac = 0.587;
     const double bFac = 0.114;

     unsigned int ROWS;
     unsigned int COLS;
     unsigned char red;
     unsigned char green;
     unsigned char blue;
     unsigned char gray_value;

     // load image

     bmpIO image1(image_path.c_str());

     // get dimensions

     ROWS = image1.height();
     COLS = image1.width();

     // create rest images

     bmpIO image2(COLS, ROWS);
     bmpIO image3(COLS, ROWS);

     // allocate matrices

     imageMatrixIn.Allocate(ROWS, COLS);
     imageMatrixOut.Allocate(ROWS, COLS);

     // create input matrix

     for (unsigned int i = 0; i < ROWS; ++i) {
          for (unsigned int j = 0; j < COLS; ++j) {
               image1.getPixel(j, i, red, green, blue);
               gray_value = rFac * red + gFac * green + bFac * blue; // get grey value
               imageMatrixIn.SetElement(i, j, gray_value);
          }
     }

     // apply the wiener filter

     wienerFilterGrayscale2D(imageMatrixIn,
                             imageMatrixOut,
                             wiener_kernel_rows,
                             wiener_kernel_cols);

     // create output image

     for (unsigned int i = 0; i < ROWS; ++i) {
          for (unsigned int j = 0; j < COLS; ++j) {
               gray_value = imageMatrixOut(i, j);
               image2.setPixel(j, i, gray_value, gray_value, gray_value);
          }
     }

     // create diff image

     for (unsigned int i = 0; i < ROWS; ++i) {
          for (unsigned int j = 0; j < COLS; ++j) {

               //gray_value = fabs(imageMatrixOut(i,j) - imageMatrixIn(i,j));

               gray_value = -imageMatrixOut(i,j) + imageMatrixIn(i,j);

               image3.setPixel(j, i, gray_value, gray_value, gray_value);
          }
     }

     // save images

     image2.saveImage(result_path);
     image3.saveImage(diff_path);

     // deallocate matrices

     imageMatrixIn.Deallocate();
     imageMatrixOut.Deallocate();

     // return

     return 0;
}

// END
