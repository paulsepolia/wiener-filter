#!/bin/bash

  # 1. compile

  g++  -O3                         \
       -Wall                       \
       -std=c++0x                  \
	  -fopenmp                    \
       wienerFilterGrayscale2D.cpp \
       driver_program_diff.cpp     \
       /opt/opencv/249/lib/*       \
       -o x_gnu
