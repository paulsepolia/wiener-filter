//========//
// Driver //
//========//

// C++

#include <iostream>
#include <string>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::string;

using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// pgg

#include "../bmp_io/bmpIO.h"
#include "wienerFilterGrayscale2D.h"
#include "../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// the main function

int main(int argc, char ** argv)
{
     // input variables

     string image_name_a = argv[1];
     string image_name_b = argv[2];
     string diff_name = argv[3];

     // local parameters and variables

     const string image_path_a = image_name_a;
     const string image_path_b = image_name_b;
     const string diff_path = diff_name;
     MatrixDense<double> imageMatrixA;
     MatrixDense<double> imageMatrixB;

     unsigned int ROWS;
     unsigned int COLS;
     unsigned char gray_value;
     unsigned char red;
     unsigned char green;
     unsigned char blue;

     // load image

     bmpIO imageA(image_path_a.c_str());
     bmpIO imageB(image_path_b.c_str());

     // get dimensions

     ROWS = imageA.height();
     COLS = imageA.width();

     // create rest images

     bmpIO imageC(COLS, ROWS);

     // allocate matrices

     imageMatrixA.Allocate(ROWS, COLS);
     imageMatrixB.Allocate(ROWS, COLS);

     // create image matrix A

     for (unsigned int i = 0; i < ROWS; ++i) {
          for (unsigned int j = 0; j < COLS; ++j) {
               imageA.getPixel(j, i, red, green, blue);
               gray_value = 1/3.0 * red + 1/3.0 * green + 1/3.0 * blue; // get grey value
               imageMatrixA.SetElement(i, j, gray_value);
          }
     }

     // create image matrix B

     for (unsigned int i = 0; i < ROWS; ++i) {
          for (unsigned int j = 0; j < COLS; ++j) {
               imageB.getPixel(j, i, red, green, blue);
               gray_value = 1/3.0 * red + 1/3.0 * green + 1/3.0 * blue; // get grey value
               imageMatrixB.SetElement(i, j, gray_value);
          }
     }

     // create diff image

     for (unsigned int i = 0; i < ROWS; ++i) {
          for (unsigned int j = 0; j < COLS; ++j) {

               gray_value = fabs(imageMatrixA(i,j) - imageMatrixB(i,j));
               imageC.setPixel(j, i, gray_value, gray_value, gray_value);
          }
     }

     // save images

     imageC.saveImage(diff_path);

     // deallocate matrices

     imageMatrixA.Deallocate();
     imageMatrixB.Deallocate();

     // return

     return 0;
}

// END
