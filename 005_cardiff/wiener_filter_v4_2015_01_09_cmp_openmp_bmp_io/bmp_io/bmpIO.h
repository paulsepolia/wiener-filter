
#ifndef BMP_IO_H
#define BMP_IO_H

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <string>

class bmpIO {
public:

     enum channelMode {
          rgbMode = 0,
          bgrMode = 1
     };

     enum colorPlane {
          bluePlane  = 0,
          greenPlane = 1,
          redPlane   = 2
     };

     bmpIO(const std::string& filename)
          : fileNameLoc(filename),
            dataLoc  (0),
            lengthLoc(0),
            widthLoc (0),
            heightLoc(0),
            rowIncrementLoc(0),
            bytesPerPixelLoc(0),
            channelModeLoc(bgrMode) {
          loadBitmap();
     }

     bmpIO(const unsigned int width, const unsigned int height)
          : fileNameLoc(""),
            dataLoc  (0),
            lengthLoc(0),
            widthLoc(width),
            heightLoc(height),
            rowIncrementLoc(0),
            bytesPerPixelLoc(3),
            channelModeLoc(bgrMode) {
          createBitmap();
     }

     ~bmpIO() {
          delete [] dataLoc;
     }

     // row

     inline unsigned char* row(unsigned int rowIndex) const {
          return dataLoc + (rowIndex * rowIncrementLoc);
     }

     // get pixel value

     inline void getPixel(const unsigned int x, const unsigned int y,
                          unsigned char& red,
                          unsigned char& green,
                          unsigned char& blue) {
          const unsigned int yOffset = y * rowIncrementLoc;
          const unsigned int xOffset = x * bytesPerPixelLoc;
          blue  = dataLoc[yOffset + xOffset + 0];
          green = dataLoc[yOffset + xOffset + 1];
          red   = dataLoc[yOffset + xOffset + 2];
     }

     //  set pixel value

     inline void setPixel(const unsigned int x, const unsigned int y,
                          const unsigned char red,
                          const unsigned char green,
                          const unsigned char blue) {
          const unsigned int yOffset = y * rowIncrementLoc;
          const unsigned int xOffset = x * bytesPerPixelLoc;
          dataLoc[yOffset + xOffset + 0] = blue;
          dataLoc[yOffset + xOffset + 1] = green;
          dataLoc[yOffset + xOffset + 2] = red;
     }

     // width

     inline unsigned int width() const {
          return widthLoc;
     }

     // height

     inline unsigned int height() const {
          return heightLoc;
     }

     // save image

     void saveImage(const std::string& file_name) {
          std::ofstream stream(file_name.c_str(),std::ios::binary);

          if (!stream) {
               std::cout << "Error - Could not open file "
                         << file_name << " for writing!" << std::endl;
               return;
          }

          bitmap_file_header bfh;
          bitmap_information_header bih;

          bih.width            = widthLoc;
          bih.height           = heightLoc;
          bih.bitCount        = static_cast<unsigned short>(bytesPerPixelLoc << 3);
          bih.clrImportant    =  0;
          bih.clr_used         =  0;
          bih.compression      =  0;
          bih.planes           =  1;
          bih.size             = 40;
          bih.x_pels_per_meter =  0;
          bih.y_pels_per_meter =  0;
          bih.size_image       = (((bih.width * bytesPerPixelLoc) + 3) & 0x0000FFFC) * bih.height;

          bfh.type      = 19778;
          bfh.size      = 55 + bih.size_image;
          bfh.reserved1 = 0;
          bfh.reserved2 = 0;
          bfh.off_bits  = bih.struct_size() + bfh.struct_size();

          write_bfh(stream,bfh);
          write_bih(stream,bih);

          unsigned int padding = (4 - ((3 * widthLoc) % 4)) % 4;
          char paddingData[4] = {0x0,0x0,0x0,0x0};

          for (unsigned int i = 0; i < heightLoc; ++i) {
               unsigned char* dataLocptr = dataLoc + (rowIncrementLoc * (heightLoc - i - 1));
               stream.write(reinterpret_cast<char*>(dataLocptr),
                            sizeof(unsigned char) * bytesPerPixelLoc * widthLoc);
               stream.write(paddingData,padding);
          }

          stream.close();
     }

     // data

     inline const unsigned char* data() {
          return dataLoc;
     }

     // offset

     inline unsigned int offset(const colorPlane color) {
          switch (channelModeLoc) {
          case rgbMode : {
               switch (color) {
               case redPlane   :
                    return 0;
               case greenPlane :
                    return 1;
               case bluePlane  :
                    return 2;
               default          :
                    return std::numeric_limits<unsigned int>::max();
               }
          }

          case bgrMode : {
               switch (color) {
               case redPlane   :
                    return 2;
               case greenPlane :
                    return 1;
               case bluePlane  :
                    return 0;
               default          :
                    return std::numeric_limits<unsigned int>::max();
               }
          }

          default       :
               return std::numeric_limits<unsigned int>::max();
          }
     }

private:

     // bitmap file header

     struct bitmap_file_header {
          unsigned short type;
          unsigned int   size;
          unsigned short reserved1;
          unsigned short reserved2;
          unsigned int   off_bits;

          unsigned int struct_size() {
               return sizeof(type)      +
                      sizeof(size)      +
                      sizeof(reserved1) +
                      sizeof(reserved2) +
                      sizeof(off_bits);
          }
     };

     // bitmap information header

     struct bitmap_information_header {
          unsigned int   size;
          unsigned int   width;
          unsigned int   height;
          unsigned short planes;
          unsigned short bitCount;
          unsigned int   compression;
          unsigned int   size_image;
          unsigned int   x_pels_per_meter;
          unsigned int   y_pels_per_meter;
          unsigned int   clr_used;
          unsigned int   clrImportant;

          unsigned int struct_size() {
               return sizeof(size)             +
                      sizeof(width)            +
                      sizeof(height)           +
                      sizeof(planes)           +
                      sizeof(bitCount)        +
                      sizeof(compression)      +
                      sizeof(size_image)       +
                      sizeof(x_pels_per_meter) +
                      sizeof(y_pels_per_meter) +
                      sizeof(clr_used)         +
                      sizeof(clrImportant);
          }
     };

     // big endian

     inline bool big_endian() {
          unsigned int v = 0x01;

          return (1 != reinterpret_cast<char*>(&v)[0]);
     }

     // flip # 1

     inline unsigned short flip(const unsigned short& v) {
          return ((v >> 8) | (v << 8));
     }

     // flip # 2

     inline unsigned int flip(const unsigned int& v) {
          return (((v & 0xFF000000) >> 0x18) |
                  ((v & 0x000000FF) << 0x18) |
                  ((v & 0x00FF0000) >> 0x08) |
                  ((v & 0x0000FF00) << 0x08));
     }

     // read from stream

     template<typename T>
     inline void readFromS(std::ifstream& stream,T& t) {
          stream.read(reinterpret_cast<char*>(&t),sizeof(T));
     }

     // write to stream

     template<typename T>
     inline void writeToS(std::ofstream& stream,const T& t) {
          stream.write(reinterpret_cast<const char*>(&t),sizeof(T));
     }

     // read header

     inline void read_bfh(std::ifstream& stream, bitmap_file_header& bfh) {
          readFromS(stream,bfh.type);
          readFromS(stream,bfh.size);
          readFromS(stream,bfh.reserved1);
          readFromS(stream,bfh.reserved2);
          readFromS(stream,bfh.off_bits);

          if (big_endian()) {
               bfh.type      = flip(bfh.type);
               bfh.size      = flip(bfh.size);
               bfh.reserved1 = flip(bfh.reserved1);
               bfh.reserved2 = flip(bfh.reserved2);
               bfh.off_bits  = flip(bfh.off_bits);
          }
     }

     // write header

     inline void write_bfh(std::ofstream& stream, const bitmap_file_header& bfh) {
          if (big_endian()) {
               writeToS(stream,flip(bfh.type     ));
               writeToS(stream,flip(bfh.size     ));
               writeToS(stream,flip(bfh.reserved1));
               writeToS(stream,flip(bfh.reserved2));
               writeToS(stream,flip(bfh.off_bits ));
          } else {
               writeToS(stream,bfh.type     );
               writeToS(stream,bfh.size     );
               writeToS(stream,bfh.reserved1);
               writeToS(stream,bfh.reserved2);
               writeToS(stream,bfh.off_bits );
          }
     }

     // read

     inline void read_bih(std::ifstream& stream,bitmap_information_header& bih) {
          readFromS(stream,bih.size  );
          readFromS(stream,bih.width );
          readFromS(stream,bih.height);
          readFromS(stream,bih.planes);
          readFromS(stream,bih.bitCount);
          readFromS(stream,bih.compression);
          readFromS(stream,bih.size_image);
          readFromS(stream,bih.x_pels_per_meter);
          readFromS(stream,bih.y_pels_per_meter);
          readFromS(stream,bih.clr_used);
          readFromS(stream,bih.clrImportant);

          if (big_endian()) {
               bih.size        = flip(bih.size     );
               bih.width       = flip(bih.width    );
               bih.height      = flip(bih.height   );
               bih.planes      = flip(bih.planes   );
               bih.bitCount   = flip(bih.bitCount);
               bih.compression = flip(bih.compression);
               bih.size_image  = flip(bih.size_image);
               bih.x_pels_per_meter = flip(bih.x_pels_per_meter);
               bih.y_pels_per_meter = flip(bih.y_pels_per_meter);
               bih.clr_used = flip(bih.clr_used);
               bih.clrImportant = flip(bih.clrImportant);
          }
     }

     // write

     inline void write_bih(std::ofstream& stream, const bitmap_information_header& bih) {
          if (big_endian()) {
               writeToS(stream,flip(bih.size));
               writeToS(stream,flip(bih.width));
               writeToS(stream,flip(bih.height));
               writeToS(stream,flip(bih.planes));
               writeToS(stream,flip(bih.bitCount));
               writeToS(stream,flip(bih.compression));
               writeToS(stream,flip(bih.size_image));
               writeToS(stream,flip(bih.x_pels_per_meter));
               writeToS(stream,flip(bih.y_pels_per_meter));
               writeToS(stream,flip(bih.clr_used));
               writeToS(stream,flip(bih.clrImportant));
          } else {
               writeToS(stream,bih.size);
               writeToS(stream,bih.width);
               writeToS(stream,bih.height);
               writeToS(stream,bih.planes);
               writeToS(stream,bih.bitCount);
               writeToS(stream,bih.compression);
               writeToS(stream,bih.size_image);
               writeToS(stream,bih.x_pels_per_meter);
               writeToS(stream,bih.y_pels_per_meter);
               writeToS(stream,bih.clr_used);
               writeToS(stream,bih.clrImportant);
          }
     }

     // create bitmap

     void createBitmap() {
          lengthLoc        = widthLoc * heightLoc * bytesPerPixelLoc;
          rowIncrementLoc = widthLoc * bytesPerPixelLoc;

          if (0 != dataLoc) {
               delete[] dataLoc;
          }

          dataLoc = new unsigned char[lengthLoc];
     }

     // load bitmap

     void loadBitmap() {
          std::ifstream stream(fileNameLoc.c_str(),std::ios::binary);

          if (!stream) {
               std::cerr << "ERROR: file "
                         << fileNameLoc << " not found!" << std::endl;
               return;
          }

          bitmap_file_header bfh;
          bitmap_information_header bih;

          read_bfh(stream,bfh);
          read_bih(stream,bih);

          if (bfh.type != 19778) {
               stream.close();
               std::cerr << "Invalid type value "
                         << bfh.type << " expected 19778." << std::endl;
               return;
          }

          if (bih.bitCount != 24) {
               stream.close();
               std::cerr << "Invalid bit depth "
                         << bih.bitCount << " expected 24." << std::endl;

               return;
          }

          heightLoc = bih.height;
          widthLoc  = bih.width;

          bytesPerPixelLoc = bih.bitCount >> 3;

          unsigned int padding = (4 - ((3 * widthLoc) % 4)) % 4;
          char paddingData[4] = {0,0,0,0};

          createBitmap();

          for (unsigned int i = 0; i < heightLoc; ++i) {
               unsigned char* dataLocPtr = row(heightLoc - i - 1); // read in inverted row order

               stream.read(reinterpret_cast<char*>(dataLocPtr),sizeof(char) * bytesPerPixelLoc * widthLoc);
               stream.read(paddingData,padding);
          }
     }

     // local variables

     std::string    fileNameLoc;
     unsigned char* dataLoc;
     unsigned int   lengthLoc;
     unsigned int   widthLoc;
     unsigned int   heightLoc;
     unsigned int   rowIncrementLoc;
     unsigned int   bytesPerPixelLoc;
     channelMode    channelModeLoc;
};

#endif // BMP_IO_H
