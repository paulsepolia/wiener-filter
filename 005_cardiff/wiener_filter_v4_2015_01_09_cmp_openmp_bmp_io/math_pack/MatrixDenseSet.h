
#ifndef MATRIX_DENSE_SET_H
#define MATRIX_DENSE_SET_H

namespace pgg {

// Set # 1

template<typename T1>
void MatrixDense<T1>::Set(const MatrixDense<T1> & mat)
{
     // test for matrices compatibility

     this->CheckCompatibility(mat);

     // main code

     unsigned int i;
     unsigned int j;

     const auto COLS = mat.cols;
     const auto ROWS = mat.rows;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);
     unsigned long index;

     #pragma omp parallel default(none) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(mat)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->mat_ptr[index] = mat.mat_ptr[index];
               }
          }
     }
}

// Set # 2

template<typename T1>
void MatrixDense<T1>::Set(const T1 & val)
{
     unsigned int i;
     unsigned int j;

     const auto COLS = this->cols;
     const auto ROWS = this->rows;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);
     unsigned long index;

     #pragma omp parallel default(none) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(val)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->mat_ptr[index] = static_cast<T1>(val);
               }
          }
     }
}

// SetElement

template<typename T1>
void MatrixDense<T1>::SetElement(const unsigned int & i_row,
                                 const unsigned int & j_col,
                                 const T1 & elem)
{
     // get the number of columns

     const auto COLS = this->cols;
     const auto COLS_LONG = static_cast<unsigned long>(COLS);

     // set the element (i_row, j_col)

     this->mat_ptr[i_row * COLS_LONG + j_col] = elem;
}

} // pgg

#endif // MATRIX_DENSE_SET_H

