//===================================//
// cvWiener2 function implementation //
//===================================//

#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::showpos;
using std::showpoint;
using std::setprecision;
using std::fixed;

#include "cv.h"
#include "cv_wiener_2.h"
#include <cstdio>

// function definition

void cvWiener2(const void* srcArr,
               void* dstArr,
               int szWindowX,
               int szWindowY)
{
     CV_FUNCNAME("cvWiener2");

     int nRows;
     int nCols;
     CvMat *p_kernel = NULL;
     CvMat srcStub;
     CvMat *srcMat = NULL;
     CvMat *p_tmpMat1;
     CvMat *p_tmpMat2;
     CvMat *p_tmpMat3;
     CvMat *p_tmpMat4;
     double noise_power;

	// trace variables

     int TR1 = 200U;
	int i;
	int j;

     __BEGIN__;

     // do checking

     if (srcArr == NULL) {
          CV_ERROR(CV_StsNullPtr, "Source array null");
     }
     if (dstArr == NULL) {
          CV_ERROR(CV_StsNullPtr, "Dest. array null");
     }

	// create wiener filter matrix kernel

     nRows = szWindowY;
     nCols = szWindowX;

     p_kernel = cvCreateMat(nRows, nCols, CV_32F);

     CV_CALL(cvSet(p_kernel, cvScalar(1.0/static_cast<double>(nRows*nCols))));



	//=============================================================================
	// trace the code --> 1 --> starts

	cout << " --> trace --> 1" << endl;

	cout << fixed;
	cout << setprecision(10);
	cout << showpos;
	cout << showpoint;

     for(i = 0; i < nRows; ++i)
	{
		for (j = 0; j < nCols; ++j)
		{
			CvScalar scal = cvGet2D(p_kernel, i,j);

			cout << " --> p_kernel = " << scal.val[0] << ", " 
				           		  << scal.val[1] << ", " 
                               		  << scal.val[2] << endl;
		}
	}

	// trace the code --> 1 --> ends
	//=============================================================================



     // convert to matrices

     srcMat = (CvMat*) srcArr;



	//=============================================================================
	// trace the code --> 2 --> starts

	cout << " --> trace --> 2" << endl;

     cout << " --> srcMat->rows = " << srcMat->rows << endl;
     cout << " --> srcMat->cols = " << srcMat->cols << endl;

     for(i = 0; i < srcMat->rows; ++i)
	{
		for (j = 0; j < srcMat->cols; ++j)
		{
			CvScalar scal = cvGet2D(srcMat, i, j);

			cout << " --> srcMat = " << scal.val[0] << ", "
				                    << scal.val[1] << ", "
                                        << scal.val[2] << endl;
		}
	}
  
     // trace the code --> 2 --> ends
	//=============================================================================



     if (!CV_IS_MAT(srcArr)) {
          CV_CALL(srcMat = cvGetMat(srcMat, &srcStub, 0, 1));
     }



	//=============================================================================
	// trace the code --> 3 --> starts

	cout << " --> trace --> 3" << endl;

     cout << " --> srcMat->rows = " << srcMat->rows << endl;
     cout << " --> srcMat->cols = " << srcMat->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(srcMat, i, j);

			cout << " --> srcMat = " << i << ", " << j << ", " 
                                        << scal.val[0] << ", "
				                    << scal.val[1] << ", "
                                        << scal.val[2] << endl;
		}
	}
  
     // trace the code --> 3 --> ends
	//=============================================================================



     // now create a temporary holding matrix

     p_tmpMat1 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat2 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat3 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat4 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));



	//=============================================================================
	// trace the code --> 4 --> starts

	cout << " --> trace --> 4" << endl;

     cout << " --> p_tmpMat1->rows = " << p_tmpMat1->rows << endl;
     cout << " --> p_tmpMat1->cols = " << p_tmpMat1->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat1, i, j);

			cout << " --> p_tmpMat1 = " << i << ", " << j << ", " 
                                           << scal.val[0] << ", "
				                       << scal.val[1] << ", "
                                           << scal.val[2] << endl;
		}
	}
  
     // trace the code --> 4 --> ends
	//=============================================================================



     // local mean of input



	//=============================================================================
	// trace the code --> 5 --> starts

	cout << " --> trace --> 5" << endl;

     cout << " --> cvPoint(nCols/2, nRows/2).x = " << cvPoint(nCols/2, nRows/2).x << endl;
     cout << " --> cvPoint(nCols/2, nRows/2).y = " << cvPoint(nCols/2, nRows/2).y << endl;

     // trace the code --> 5 --> ends
	//=============================================================================



     cvFilter2D(srcMat, p_tmpMat1, p_kernel, cvPoint(nCols/2, nRows/2)); // localMean



	//=============================================================================
	// trace the code --> 6 --> starts

	cout << " --> trace --> 6" << endl;

     cout << " --> p_tmpMat1->rows = " << p_tmpMat1->rows << endl;
     cout << " --> p_tmpMat1->cols = " << p_tmpMat1->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat1, i, j);

			cout << " --> p_tmpMat1 = " << i << ", " << j << ", " 
                                           << scal.val[0] << ", "
				                       << scal.val[1] << ", "
                                           << scal.val[2] << endl;
		}
	}

	// trace the code --> 6 --> ends
	//=============================================================================
 


     // local variance of input

     cvMul(srcMat, srcMat, p_tmpMat2); // in^2



	//=============================================================================
	// trace the code --> 7 --> starts

	cout << " --> trace --> 7" << endl;

     cout << " --> p_tmpMat2->rows = " << p_tmpMat2->rows << endl;
     cout << " --> p_tmpMat2->cols = " << p_tmpMat2->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat2, i, j);

			cout << " --> p_tmp_mat2 = " << i << ", " << j << ", " 
                                            << scal.val[0] << ", "
				                        << scal.val[1] << ", "
                                            << scal.val[2] << endl;
		}
	}

	// trace the code --> 7 --> ends
	//=============================================================================



     cvFilter2D(p_tmpMat2, p_tmpMat3, p_kernel, cvPoint(nCols/2, nRows/2));



	//=============================================================================
	// trace the code --> 8 --> starts

	cout << " --> trace --> 8" << endl;

     cout << " --> p_tmpMat3->rows = " << p_tmpMat3->rows << endl;
     cout << " --> p_tmpMat3->cols = " << p_tmpMat3->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat3, i, j);

			cout << " --> p_tmp_mat3 = " << i << ", " << j << ", " 
                                            << scal.val[0] << ", "
				                        << scal.val[1] << ", "
                                            << scal.val[2] << endl;
		}
	}

	// trace the code --> 8 --> ends
	//=============================================================================



     // subtract off local_mean^2 from local variance

     cvMul(p_tmpMat1, p_tmpMat1, p_tmpMat4); // localMean^2



	//=============================================================================
	// trace the code --> 9 --> starts

	cout << " --> trace --> 9" << endl;

     cout << " --> p_tmpMat4->rows = " << p_tmpMat4->rows << endl;
     cout << " --> p_tmpMat4->cols = " << p_tmpMat4->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat4, i, j);

			cout << " --> p_tmp_mat4 = " << i << ", " << j << ", " 
                                            << scal.val[0] << ", "
				                        << scal.val[1] << ", "
                                            << scal.val[2] << endl;
		}
	}

	// trace the code --> 9 --> ends
	//=============================================================================



     cvSub(p_tmpMat3, p_tmpMat4, p_tmpMat3); // filter(in^2) - localMean^2 ==> localVariance



	//=============================================================================
	// trace the code --> 10 --> starts

	cout << " --> trace --> 10" << endl;

     cout << " --> p_tmpMat3->rows = " << p_tmpMat3->rows << endl;
     cout << " --> p_tmpMat3->cols = " << p_tmpMat3->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat3, i, j);

			cout << " --> p_tmp_mat3 = " << i << ", " << j << ", " 
                                            << scal.val[0] << ", "
				                        << scal.val[1] << ", "
                                            << scal.val[2] << endl;
		}
	}

	// trace the code --> 10 --> ends
	//=============================================================================



     // estimate noise power

     noise_power = cvMean(p_tmpMat3, 0);


	//=============================================================================
	// trace the code --> 11 --> starts

	cout << " --> trace --> 11" << endl;

	cout << " --> noise_power = " << noise_power << endl;

	// trace the code --> 11 --> ends
	//=============================================================================




     // result = local_mean  + (max(0, localVar - noise) ./ max(localVar, noise)) .* (in - local_mean)

     cvSub(srcMat, p_tmpMat1, dstArr); //in - local_mean



	//=============================================================================
	// trace the code --> 12 --> starts

	cout << " --> trace --> 12 " << endl;

//     cout << " --> dstArr->rows = " << dstArr->rows << endl;
//     cout << " --> dstArr->cols = " << dstArr->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(dstArr, i, j);

			cout << " --> dstArr = " << i << ", " << j << ", " 
                                        << scal.val[0] << ", "
				                    << scal.val[1] << ", "
                                        << scal.val[2] << endl;
		}
	}

	// trace the code --> 12 --> ends
	//=============================================================================



     cvMaxS(p_tmpMat3, noise_power, p_tmpMat2); // max(localVar, noise)



	//=============================================================================
	// trace the code --> 13 --> starts

	cout << " --> trace --> 13" << endl;

     cout << " --> p_tmpMat2->rows = " << p_tmpMat2->rows << endl;
     cout << " --> p_tmpMat2->cols = " << p_tmpMat2->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat2, i, j);

			cout << " --> p_tmp_mat2 = " << i << ", " << j << ", " 
                                            << scal.val[0] << ", "
				                        << scal.val[1] << ", "
                                            << scal.val[2] << endl;
		}
	}

	// trace the code --> 13 --> ends
	//=============================================================================



     cvAddS(p_tmpMat3, cvScalar(-noise_power), p_tmpMat3); // localVar - noise



	//=============================================================================
	// trace the code --> 14 --> starts

	cout << " --> trace --> 14" << endl;

     cout << " --> p_tmpMat3->rows = " << p_tmpMat3->rows << endl;
     cout << " --> p_tmpMat3->cols = " << p_tmpMat3->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat3, i, j);

			cout << " --> p_tmp_mat3 = " << i << ", " << j << ", " 
                                            << scal.val[0] << ", "
				                        << scal.val[1] << ", "
                                            << scal.val[2] << endl;
		}
	}

	// trace the code --> 14 --> ends
	//=============================================================================


     cvMaxS(p_tmpMat3, 0, p_tmpMat3); // max(0, localVar - noise)


	//=============================================================================
	// trace the code --> 15 --> starts

	cout << " --> trace --> 15" << endl;

     cout << " --> p_tmpMat3->rows = " << p_tmpMat3->rows << endl;
     cout << " --> p_tmpMat3->cols = " << p_tmpMat3->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat3, i, j);

			cout << " --> p_tmp_mat3 = " << i << ", " << j << ", " 
                                            << scal.val[0] << ", "
				                        << scal.val[1] << ", "
                                            << scal.val[2] << endl;
		}
	}

	// trace the code --> 15 --> ends
	//=============================================================================



     cvDiv(p_tmpMat3, p_tmpMat2, p_tmpMat3); //max(0, localVar-noise) / max(localVar, noise)



	//=============================================================================
	// trace the code --> 16 --> starts

	cout << " --> trace --> 16" << endl;

     cout << " --> p_tmpMat3->rows = " << p_tmpMat3->rows << endl;
     cout << " --> p_tmpMat3->cols = " << p_tmpMat3->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(p_tmpMat3, i, j);

			cout << " --> p_tmp_mat3 = " << i << ", " << j << ", " 
                                            << scal.val[0] << ", "
				                        << scal.val[1] << ", "
                                            << scal.val[2] << endl;
		}
	}

	// trace the code --> 16 --> ends
	//=============================================================================



     cvMul(p_tmpMat3, dstArr, dstArr);



	//=============================================================================
	// trace the code --> 17 --> starts

	cout << " --> trace --> 17" << endl;

//     cout << " --> dstArr->rows = " << dstArr->rows << endl;
//     cout << " --> dstArr->cols = " << dstArr->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(dstArr, i, j);

			cout << " --> dstArr = " << i << ", " << j << ", " 
                                        << scal.val[0] << ", "
				                    << scal.val[1] << ", "
                                        << scal.val[2] << endl;
		}
	}

	// trace the code --> 17 --> ends
	//=============================================================================

     cvAdd(dstArr, p_tmpMat1, dstArr);

	//=============================================================================
	// trace the code --> 18 --> starts

	cout << " --> trace --> 18" << endl;

//     cout << " --> dstArr->rows = " << dstArr->rows << endl;
//     cout << " --> dstArr->cols = " << dstArr->cols << endl;

     for(i = 0; i < TR1; ++i)
	{
		for (j = 0; j < TR1; ++j)
		{
			CvScalar scal = cvGet2D(dstArr, i, j);

			cout << " --> dstArr = " << i << ", " << j << ", " 
                                        << scal.val[0] << ", "
				                    << scal.val[1] << ", "
                                        << scal.val[2] << endl;
		}
	}

	// trace the code --> 18 --> ends
	//=============================================================================
     // free up reserved RAM

     cvReleaseMat(&p_kernel);
     cvReleaseMat(&p_tmpMat1);
     cvReleaseMat(&p_tmpMat2);
     cvReleaseMat(&p_tmpMat3);
     cvReleaseMat(&p_tmpMat4);

     __END__;
}

// FINI
