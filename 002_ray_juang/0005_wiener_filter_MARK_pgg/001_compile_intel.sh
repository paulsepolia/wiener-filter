#!/bin/bash

  # 1. compile

  icpc -O3                   \
       -Wall                 \
       -std=c++11            \
	  -openmp               \
       cv_wiener_2.cpp       \
       driver_program.cpp    \
       /opt/opencv/249/lib/* \
       -o x_intel
