//========//
// Driver //
//========//

#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

#include "cv.h"
#include "highgui.h"

using namespace cv;

#include "wiener_filter_grayscale.h"

// the main code

int main()
{
     // input variables

     const string image_name = "image_7.tif";
     const string result_name = "result_7.tif";
     const string image_dir = "../../../not_monitored/images";
     const string result_dir = "../../../not_monitored/results";

     // local parameters and variables

     const string image_path = image_dir + "/" + image_name;
     const string result_path = result_dir + "/" + result_name;

     // load image

     IplImage *tmp = cvLoadImage(image_path.c_str());

     // create image

     IplImage *tmp2 = cvCreateImage(cvSize(tmp->width, tmp->height), IPL_DEPTH_8U, 1);

     // convert to grayscale

     cvCvtColor(tmp, tmp2, CV_RGB2GRAY);

     // apply the wiener filter

     wiener_filter_grayscale(tmp2, tmp2, 10, 10);

     // save image

     cvSaveImage(result_path.c_str(), tmp2);

     // free up RAM

     cvReleaseImage(&tmp);
     cvReleaseImage(&tmp2);

     // sentineling

     int sentinel;
     cout << " --> Enter an integer to exit: ";
     cin >> sentinel;

     // return

     return 0;
}

// FINI
