#!/bin/bash

  # 1. compile

  g++  -O3                         \
       -Wall                       \
       -std=c++0x                  \
       wiener_filter_grayscale.cpp \
       driver_program.cpp          \
       /opt/opencv/249/lib/*       \
       -fopenmp                    \
       -o x_gnu
