//=================================================//
// wiener_filter_grayscale function implementation //
//=================================================//

// C++

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <utility>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::pair;

// OpenCV

#include "cv.h"

// pgg

#include "wiener_filter_grayscale.h"
#include "../../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// function definition

void wiener_filter_grayscale(const void* srcArr,
                             void* dstArr,
                             int szWindowX,
                             int szWindowY)
{
     CV_FUNCNAME("cvWiener2");

     int nRows;
     int nCols;
     CvMat *p_kernel = NULL;
     CvMat srcStub;
     CvMat *srcMat = NULL;
     CvMat *p_tmpMat1;
     CvMat *p_tmpMat2;
     CvMat *p_tmpMat3;
     CvMat *p_tmpMat4;
     double noise_power;

     // pgg --> starts

     MatrixDense<double> kernelAlpha;
     MatrixDense<double> imageMatrixA;
     MatrixDense<double> mat_tmp_1;
     MatrixDense<double> mat_tmp_2;
     MatrixDense<double> mat_tmp_3;
     MatrixDense<double> mat_tmp_4;
     MatrixDense<double> destArray;

     unsigned int ROWS;
     unsigned int COLS;
     unsigned int i;
     unsigned int j;
     double tmp_double1;
     double tmp_double2;
     double noisePower;
     const double THRES_A = static_cast<double>(pow(10.0, -6.0));
     pair<int, int> anchor;

     // pgg --> ends

     __BEGIN__;

     // adjust output

     cout << fixed;
     cout << setprecision(10);
     cout << showpos;
     cout << showpoint;

     // DO CHECKING

     if (srcArr == NULL) {
          CV_ERROR(CV_StsNullPtr, "Source array null");
     }
     if (dstArr == NULL) {
          CV_ERROR(CV_StsNullPtr, "Dest. array null");
     }

     nRows = szWindowY;
     nCols = szWindowX;


     // STEP --> 1 --> DONE

     // opencv way

     p_kernel = cvCreateMat(nRows, nCols, CV_32F);
     CV_CALL(cvSet(p_kernel, cvScalar(1.0 / static_cast<double>(nRows * nCols))));

     // pgg way

     kernelAlpha.Allocate(nRows, nCols);
     kernelAlpha = 1.0/static_cast<double>(nRows*nCols);


     // STEP --> 2 --> NOT DONE

     // opencv way

     srcMat = (CvMat*) srcArr;

     if (!CV_IS_MAT(srcArr)) {
          CV_CALL(srcMat = cvGetMat(srcMat, &srcStub, 0, 1));
     }

     // pgg way

     ROWS = srcMat->rows;
     COLS = srcMat->cols;

     imageMatrixA.Allocate(ROWS, COLS);

     for(i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               CvScalar scal = cvGet2D(srcMat, i, j);
               imageMatrixA.SetElement(i, j, scal.val[0]);
          }
     }

     // test full

     for(i = 0; i < ROWS; ++i) {
          for (j = 0; j < COLS; ++j) {
               CvScalar scal = cvGet2D(srcMat, i, j);
               tmp_double1 = imageMatrixA(i,j);
               tmp_double2 = static_cast<double>(scal.val[0]);

               if (abs(tmp_double1 - tmp_double2) > THRES_A) {
                    cout << "ERROR --> S2 --> " << i << ", " << j << endl;
                    break;
               }
          }
     }

     // Now create a temporary holding matrix

     p_tmpMat1 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat2 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat3 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat4 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));


     // STEP --> 3 --> DONE

     // opencv way

     cvFilter2D(srcMat, p_tmpMat1, p_kernel, cvPoint(nCols/2, nRows/2));

     // pgg way

     mat_tmp_1.Allocate(ROWS, COLS);

     anchor.first = nCols/2;
     anchor.second = nRows/2;
     mat_tmp_1.MatrixCorrelate(kernelAlpha, anchor);


     // STEP --> 4 --> DONE

     // opencv way

     cvMul(srcMat, srcMat, p_tmpMat2);

     // pgg way

     mat_tmp_2.Allocate(ROWS, COLS);
     mat_tmp_2.Times(imageMatrixA, imageMatrixA);
     mat_tmp_2.SetMax(255.0, 255.0);


     // STEP --> 5 --> DONE

     // opencv way

     cvFilter2D(p_tmpMat2, p_tmpMat3, p_kernel, cvPoint(nCols / 2, nRows / 2));

     // pgg way

     anchor.first = nCols/2;
     anchor.second = nRows/2;
     mat_tmp_3.Allocate(ROWS, COLS);
     mat_tmp_3.MatrixCorrelate(mat_tmp_2, kernelAlpha, anchor);


     // STEP --> 6 --> DONE

     // opencv way

     cvMul(p_tmpMat1, p_tmpMat1, p_tmpMat4);

     // pgg way

     mat_tmp_4.Allocate(ROWS, COLS);
     mat_tmp_4.Times(mat_tmp_1, mat_tmp_1);
     mat_tmp_4.SetMax(255.0, 255.0);


     // STEP --> 7 --> DONE

     // opencv way

     cvSub(p_tmpMat3, p_tmpMat4, p_tmpMat3);

     // pgg way

     mat_tmp_3.Subtract(mat_tmp_3, mat_tmp_4);
     mat_tmp_3.SetMax(255.0, 255.0);
     mat_tmp_3.SetMin(0.0, 0.0);


     // STEP --> 8 --> DONE

     // opencv way

     noise_power = cvMean(p_tmpMat3, 0);

     // pgg way

     noisePower = mat_tmp_3.Mean();


     // STEP --> 9 --> DONE

     // opencv way

     cvSub(srcMat, p_tmpMat1, dstArr);

     // pgg way

     destArray.Allocate(ROWS, COLS);
     destArray.Subtract(imageMatrixA, mat_tmp_1);
     destArray.SetMax(255.0, 255.0);
     destArray.SetMin(0.0, 0.0);


     // STEP --> 10 --> DONE

     // opencv way

     cvMaxS(p_tmpMat3, noise_power, p_tmpMat2);

     // pgg way

     mat_tmp_2.SetMax(mat_tmp_3, noisePower);
     mat_tmp_2.SetMax(255.0, 255.0);
     mat_tmp_2.SetMin(0.0, 0.0);


     // STEP --> 11 --> DONE

     // opencv way

     cvAddS(p_tmpMat3, cvScalar(-noise_power), p_tmpMat3);

     // pgg way

     mat_tmp_3.Plus(mat_tmp_3, -noisePower);
     mat_tmp_3.SetMax(255.0, 255.0);
     mat_tmp_3.SetMin(0.0, 0.0);


     // STEP --> 12 --> DONE

     // opencv way

     cvMaxS(p_tmpMat3, 0, p_tmpMat3);

     // pgg way

     mat_tmp_3.SetMax(mat_tmp_3, 0);
     mat_tmp_3.SetMax(255.0, 255.0);
     mat_tmp_3.SetMin(0.0, 0.0);


     // STEP --> 13 --> DONE

     // opencv way

     cvDiv(p_tmpMat3, p_tmpMat2, p_tmpMat3);

     // pgg way

     mat_tmp_3.Divide(mat_tmp_3, mat_tmp_2);
     mat_tmp_3.SetMax(255.0, 255.0);
     mat_tmp_3.SetMin(0.0, 0.0);


     // STEP --> 14 --> DONE

     // opencv way

     cvMul(p_tmpMat3, dstArr, dstArr);

     // pgg way

     destArray.Times(mat_tmp_3, destArray);
     destArray.SetMax(255.0, 255.0);
     destArray.SetMin(0.0, 0.0);


     // STEP --> 15 --> DONE

     // opencv way

     cvAdd(dstArr, p_tmpMat1, dstArr);

     // pgg way

     destArray.Plus(destArray, mat_tmp_1);

     // remains to return the destArray
     // NOT the dstArr

     // deallocate matrices

     mat_tmp_1.Deallocate();
     mat_tmp_2.Deallocate();
     mat_tmp_3.Deallocate();
     mat_tmp_4.Deallocate();

     // free up reserved RAM

     cvReleaseMat(&p_kernel);
     cvReleaseMat(&p_tmpMat1);
     cvReleaseMat(&p_tmpMat2);
     cvReleaseMat(&p_tmpMat3);
     cvReleaseMat(&p_tmpMat4);

     __END__;
}

// FINI
