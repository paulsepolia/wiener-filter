
#ifndef WIENER_FILTER_GRAYSCALE_H
#define WIENER_FILTER_GRAYSCALE_H

void wiener_filter_grayscale(const void* input_array,
                             void* output_array,
                             int kernel_rows = 3,
                             int kernel_cols = 3);

#endif // WIENER_FILTER_GRAYSCALE_H

// FINI
