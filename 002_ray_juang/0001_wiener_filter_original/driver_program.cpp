
void main(int argc, char *argv[])
if (argc <= 1)
{
     printf("Usage: %s <image>\n", argv[0]);
     return;
}

IplImage *tmp = cvLoadImage(argv[1]);
IplImage *tmp2 = cvCreateImage(cvSize(tmp->width, tmp->height), IPL_DEPTH_8U, 1);

cvCvtColor(tmp, tmp2, CV_RGB2GRAY);

cvNamedWindow("Before");
cvShowImage("Before", tmp);

cvWiener2(tmp2, tmp2, 3,3);
cvNamedWindow("After");

cvShowImage("After", tmp2);
//cvSaveImage("C:/temp/result.png", tmp2);
cvWaitKey(-1);


cvReleaseImage(&tmp);
cvReleaseImage(&tmp2);
}

