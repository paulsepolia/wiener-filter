//===================================//
// cvWiener2 function implementation //
//===================================//

// C++

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <utility>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::pair;

// OpenCV

#include "cv.h"
#include "cv_wiener_2.h"

// pgg

#include "../../sepolia_v17/matrix_dense/MatrixDense.h"
#include "../../sepolia_v17/functions/Functions.h"

using pgg::MatrixDense;

// function definition

void cvWiener2(const void* srcArr,
               void* dstArr,
               int szWindowX,
               int szWindowY)
{
     CV_FUNCNAME("cvWiener2");

     int nRows;
     int nCols;
     CvMat *p_kernel = NULL;
     CvMat srcStub;
     CvMat *srcMat = NULL;
     CvMat *p_tmpMat1;
     CvMat *p_tmpMat2;
     CvMat *p_tmpMat3;
     CvMat *p_tmpMat4;
     double noise_power;

	// pgg --> starts

	MatrixDense<double> kernelAlpha;
	MatrixDense<double> imageMatrixA;
	MatrixDense<double> mat_tmp_1;
	MatrixDense<double> mat_tmp_2;
	MatrixDense<double> mat_tmp_3;
	MatrixDense<double> mat_tmp_4;
	MatrixDense<double> destArray;

	unsigned int ROWS;
	unsigned int COLS;
	unsigned int i;
	unsigned int j;
	double tmp_double1;
	double tmp_double2;
	double noisePower;
	const double THRES_A = static_cast<double>(pow(10.0, -6.0));
	pair<int, int> anchor;

	// pgg --> ends

     __BEGIN__;

	// adjust output

	cout << fixed;
	cout << setprecision(10);
	cout << showpos;
	cout << showpoint;

     // DO CHECKING

     if (srcArr == NULL) {
          CV_ERROR(CV_StsNullPtr, "Source array null");
     }
     if (dstArr == NULL) {
          CV_ERROR(CV_StsNullPtr, "Dest. array null");
     }

     nRows = szWindowY;
     nCols = szWindowX;

	// STEP --> 1 --> DONE

	// create a matrix of nRows x nCols dimension
     // each element of the matrix is 1.0/(nRows x nCols)
	// name the matrix: kernelAlpha
	// elements are of type double

	// opencv way

     p_kernel = cvCreateMat(nRows, nCols, CV_32F);
     CV_CALL(cvSet(p_kernel, cvScalar(1.0 / static_cast<double>(nRows * nCols))));

	// pgg way

	kernelAlpha.Allocate(nRows, nCols);
	kernelAlpha = 1.0/static_cast<double>(nRows*nCols);

	// test full

     for(i = 0; i < static_cast<unsigned int>(nRows); ++i)
     {
          for (j = 0; j < static_cast<unsigned int>(nCols); ++j)
          {
               CvScalar scal = cvGet2D(p_kernel, i, j);
			tmp_double1 = kernelAlpha(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double1 - tmp_double2) > THRES_A)
			{
				cout << "ERROR --> S1 --> " << i << ", " << j << endl;
				break;
			}
          }
     }


	// STEP --> 2 --> NOT DONE
	// remains srcMat to be converted to my format
	// and assigned to imageMatrixA
	// and finally imageMatrixA to be input to the routine

     // Convert to matrices

	// create the matrix to hold the original image
     // must be of size srcMat->rows x srcMat->cols
     // elements are of type double for 0.0 -> 255.0

	// opencv way

     srcMat = (CvMat*) srcArr;

     if (!CV_IS_MAT(srcArr)) {
          CV_CALL(srcMat = cvGetMat(srcMat, &srcStub, 0, 1));
     }

	// pgg way

	ROWS = srcMat->rows;
	COLS = srcMat->cols;

	imageMatrixA.Allocate(ROWS, COLS);

     for(i = 0; i != ROWS; ++i)
     {
          for (j = 0; j != COLS; ++j)
          {
               CvScalar scal = cvGet2D(srcMat, i, j);
               imageMatrixA.SetElement(i, j, scal.val[0]);
          }
     }

	// test full

     for(i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(srcMat, i, j);
			tmp_double1 = imageMatrixA(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double1 - tmp_double2) > THRES_A)
			{
				cout << "ERROR --> S2 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

     // Now create a temporary holding matrix

     p_tmpMat1 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat2 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat3 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));
     p_tmpMat4 = cvCreateMat(srcMat->rows, srcMat->cols, CV_MAT_TYPE(srcMat->type));


	// STEP --> 3 --> NOT DONE
	// remains the implementation of the cvFilter2D

	// Create a matrix of dimensions of the original image
	// apply the local mean filter and put there the data

	// opencv way

     //Local mean of input

     cvFilter2D(srcMat, p_tmpMat1, p_kernel, cvPoint(nCols / 2, nRows / 2)); //localMean

	// pgg way

	mat_tmp_1.Allocate(ROWS, COLS);

     anchor.first = nCols/2;
     anchor.second = nRows/2;
     mat_tmp_1.MatrixCorrelate(mat_tmp_1, kernelAlpha, anchor);

//     for(i = 0; i != ROWS; ++i)
//     {
//          for (j = 0; j != COLS; ++j)
//          {
//               CvScalar scal = cvGet2D(p_tmpMat1, i, j);
//			mat_tmp_1.SetElement(i, j, scal.val[0]);
//          }
//     }

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat1, i, j);
			tmp_double1 = mat_tmp_1(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double1 - tmp_double2) > THRES_A)
			{
				cout << "ERROR --> S3 --> " << i << ", " << j << endl;
				break;
			}
          }
     }


	// STEP --> 4 --> DONE

	// Create a matrix of dimension of the original image
	// and multiply element by element

	// opencv way

     //Local variance of input

     cvMul(srcMat, srcMat, p_tmpMat2); //in^2

	// pgg way

	mat_tmp_2.Allocate(ROWS, COLS);
	mat_tmp_2.Times(imageMatrixA, imageMatrixA);
	mat_tmp_2.SetMax(255.0, 255.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat2, i, j);
			tmp_double1 = mat_tmp_2(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double1 - tmp_double2) > THRES_A)
			{
				cout << "ERROR --> S4 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 5 --> DONE

	// opencv way

     cvFilter2D(p_tmpMat2, p_tmpMat3, p_kernel, cvPoint(nCols / 2, nRows / 2));

	// pgg way

	anchor.first = nCols/2;
	anchor.second = nRows/2;
	mat_tmp_3.Allocate(ROWS, COLS);
	mat_tmp_3.MatrixCorrelate(mat_tmp_2, kernelAlpha, anchor);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat3, i, j);
			tmp_double1 = mat_tmp_3(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double1 - tmp_double2) > THRES_A)
			{
				cout << "ERROR --> S5 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 6 --> DONE

	// opencv way

     //Subtract off local_mean^2 from local variance

     cvMul(p_tmpMat1, p_tmpMat1, p_tmpMat4); //localMean^2

	// pgg way

	mat_tmp_4.Allocate(ROWS, COLS);
	mat_tmp_4.Times(mat_tmp_1, mat_tmp_1);
	mat_tmp_4.SetMax(255.0, 255.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat4, i, j);
			tmp_double1 = mat_tmp_4(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);			

			if (abs(tmp_double1 - tmp_double2) > THRES_A)
			{
				cout << "ERROR --> S6 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 7 --> DONE

	// opencv way

     cvSub(p_tmpMat3, p_tmpMat4, p_tmpMat3); //filter(in^2) - localMean^2 ==> localVariance

	// pgg way

	mat_tmp_3.Subtract(mat_tmp_3, mat_tmp_4);
	mat_tmp_3.SetMax(255.0, 255.0);
	mat_tmp_3.SetMin(0.0, 0.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat3, i, j);
			tmp_double1 = mat_tmp_3(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);			

			if (abs(tmp_double1 - tmp_double2) > THRES_A)
			{
				cout << "ERROR --> S7 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 8 --> DONE

     //Estimate noise power

	// opencv way

     noise_power = cvMean(p_tmpMat3, 0);

	// pgg way

	noisePower = mat_tmp_3.Mean();

     // result = local_mean  + ( max(0, localVar - noise) ./ max(localVar, noise)) .* (in - local_mean)

	// STEP --> 9 --> DONE

	// opencv way

     cvSub(srcMat, p_tmpMat1, dstArr); //in - local_mean

	// pgg way

	destArray.Allocate(ROWS, COLS);

	destArray.Subtract(imageMatrixA, mat_tmp_1);
	destArray.SetMax(255.0, 255.0);
	destArray.SetMin(0.0, 0.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(dstArr, i, j);
			tmp_double1 = destArray(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);			

			if (abs(tmp_double1 - tmp_double2) > THRES_A)
			{
				cout << "ERROR --> S8 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 10 --> DONE

	// opencv way

     cvMaxS(p_tmpMat3, noise_power, p_tmpMat2); //max(localVar, noise)

	// pgg way

	mat_tmp_2.SetMax(mat_tmp_3, noisePower);
	mat_tmp_2.SetMax(255.0, 255.0);
	mat_tmp_2.SetMin(0.0, 0.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat2, i, j);
			tmp_double1 = mat_tmp_2(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double2 - tmp_double1) > THRES_A)
			{
				cout << "ERROR --> S10 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 11 --> DONE

	// opencv way

     cvAddS(p_tmpMat3, cvScalar(-noise_power), p_tmpMat3); //localVar - noise

	// pgg way

	mat_tmp_3.Plus(mat_tmp_3, -noisePower);
	mat_tmp_3.SetMax(255.0, 255.0);
	mat_tmp_3.SetMin(0.0, 0.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat3, i, j);
			tmp_double1 = mat_tmp_3(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double2 - tmp_double1) > THRES_A)
			{
				cout << "ERROR --> S11 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 12 --> DONE

	// opencv way

     cvMaxS(p_tmpMat3, 0, p_tmpMat3); // max(0, localVar - noise)

	// pgg way

	mat_tmp_3.SetMax(mat_tmp_3, 0);
	mat_tmp_3.SetMax(255.0, 255.0);
	mat_tmp_3.SetMin(0.0, 0.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat3, i, j);
			tmp_double1 = mat_tmp_3(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double2 - tmp_double1) > THRES_A)
			{
				cout << "ERROR --> S12 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 13 --> DONE

	// opencv way

     cvDiv(p_tmpMat3, p_tmpMat2, p_tmpMat3); //max(0, localVar-noise) / max(localVar, noise)

	// pgg way

	mat_tmp_3.Divide(mat_tmp_3, mat_tmp_2);
	mat_tmp_3.SetMax(255.0, 255.0);
	mat_tmp_3.SetMin(0.0, 0.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(p_tmpMat3, i, j);
			tmp_double1 = mat_tmp_3(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double2 - tmp_double1) > THRES_A)
			{
				cout << "ERROR --> S13 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 14 --> DONE

	// opencv way

     cvMul(p_tmpMat3, dstArr, dstArr);

	// pgg way

	destArray.Times(mat_tmp_3, destArray);
	destArray.SetMax(255.0, 255.0);
	destArray.SetMin(0.0, 0.0);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(dstArr, i, j);
			tmp_double1 = destArray(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double2 - tmp_double1) > THRES_A)
			{
				cout << "ERROR --> S14 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// STEP --> 15 --> DONE

	// opencv way

     cvAdd(dstArr, p_tmpMat1, dstArr);

	// pgg way

	destArray.Plus(destArray, mat_tmp_1);

	// test full

     for (i = 0; i < ROWS; ++i)
     {
          for (j = 0; j < COLS; ++j)
          {
               CvScalar scal = cvGet2D(dstArr, i, j);
			tmp_double1 = destArray(i,j);
			tmp_double2 = static_cast<double>(scal.val[0]);

			if (abs(tmp_double2 - tmp_double1) > THRES_A)
			{
				cout << "ERROR --> S14 --> " << i << ", " << j << endl;
				break;
			}
          }
     }

	// remains to return the destArray
     // NOT the dstArr

	// deallocate matrices

	mat_tmp_1.Deallocate();
	mat_tmp_2.Deallocate();
	mat_tmp_3.Deallocate();
	mat_tmp_4.Deallocate();

     // free up reserved RAM

     cvReleaseMat(&p_kernel);
     cvReleaseMat(&p_tmpMat1);
     cvReleaseMat(&p_tmpMat2);
     cvReleaseMat(&p_tmpMat3);
     cvReleaseMat(&p_tmpMat4);

     __END__;
}

// FINI
