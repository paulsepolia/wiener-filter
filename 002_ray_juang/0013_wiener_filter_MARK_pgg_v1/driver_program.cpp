//========//
// Driver //
//========//

// C++

#include <iostream>
#include <string>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::string;

using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// OpenCV

#include "cv.h"
#include "highgui.h"

using namespace cv;

// pgg

#include "wienerFilterGrayscale2D.h"

#include "../../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// the main function

int main()
{
     // input variables

     const string image_name = "image_7.tif";
     const string result_name = "result_7.tif";
     const string image_dir = "../../../not_monitored/images";
     const string result_dir = "../../../not_monitored/results";
	const unsigned int wiener_kernel_rows = 4; // >= 2
	const unsigned int wiener_kernel_cols = 4;  // >= 2

     // local parameters and variables

     const string image_path = image_dir + "/" + image_name;
     const string result_path = result_dir + "/" + result_name;
	MatrixDense<double> imageMatrixIn;
	MatrixDense<double> imageMatrixOut;
	int ROWS;
	int COLS;
	
	// opencv variables

	CvMat matTmp1;
     CvMat *matTmp2 = NULL;

     // load image - opencv

     IplImage *im1 = cvLoadImage(image_path.c_str());

     // create image - opencv

     IplImage *im2 = cvCreateImage(cvSize(im1->width, im1->height), IPL_DEPTH_8U, 1);

     // convert to grayscale - opencv

     cvCvtColor(im1, im2, CV_RGB2GRAY);
	
	// put image to matrix - opencv
	
	matTmp2 = (CvMat*) im2;
     matTmp2 = cvGetMat(matTmp2, &matTmp1, 0, 1);

	// get the rows and cols of the image - opencv

     ROWS = matTmp2->rows;
     COLS = matTmp2->cols;

	// allocate image matrix

     imageMatrixIn.Allocate(ROWS, COLS);
     imageMatrixOut.Allocate(ROWS, COLS);

	// build image matrix - opencv and mine

     Mat matimg(im2);

	for (int i = 0; i != ROWS; ++i) {
     	for (int j = 0; j != COLS; ++j) {
               unsigned char tmp;
               tmp = ((uchar*) matimg.data + i * matimg.step)[j * matimg.elemSize() + 1];
               imageMatrixIn.SetElement(i, j, tmp);
          }
     }

     // apply the wiener filter

     wienerFilterGrayscale2D(imageMatrixIn, 
					    imageMatrixOut, 
					    wiener_kernel_rows,
                             wiener_kernel_cols);

	// opencv way to save image

	IplImage * imOut;
     CvSize imOutSize;

	imOutSize.width = COLS;
     imOutSize.height = ROWS;

	imOut = cvCreateImage(imOutSize, 8, 1);

     for (int i = 0; i != ROWS; ++i) {
          for (int j = 0; j != COLS; ++j) {
               ((uchar*)(imOut->imageData + imOut->widthStep*i))[j] =
                    static_cast<char>(imageMatrixOut(i,j));
          }
     }

     // save image - opencv

     cvSaveImage(result_path.c_str(), imOut);

     // free up RAM - opencv

     cvReleaseImage(&im1);
     cvReleaseImage(&im2);
     cvReleaseImage(&imOut);

	// deallocate matrices

	imageMatrixIn.Deallocate();
	imageMatrixOut.Deallocate();

     // sentineling

     int sentinel;
     cout << " Enter an integer to exit: ";
     cin >> sentinel;

     // return

     return 0;
}

// END
