//========//
// Driver //
//========//

// C++

#include <iostream>
#include <string>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::string;

using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// OpenCV

#include "cv.h"
#include "highgui.h"

using namespace cv;

// pgg

#include "wienerFilterGrayscale2D.h"

#include "../../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// the main function

int main()
{
     // input variables

     const string image_name = "image_7.tif";
     const string result_name = "result_7.tif";
     const string image_dir = "../../../not_monitored/images";
     const string result_dir = "../../../not_monitored/results";

     // local parameters and variables

     const string image_path = image_dir + "/" + image_name;
     const string result_path = result_dir + "/" + result_name;

	CvMat srcStub;
     CvMat *srcMat = NULL;

	MatrixDense<double> imageMatrixA;
	MatrixDense<double> imageMatrixB;

	// adjust the output format

	cout << fixed;
     cout << setprecision(16);
     cout << showpos;
     cout << showpoint;

     // load image

     IplImage *tmp = cvLoadImage(image_path.c_str());

     // create image

     IplImage *tmp2 = cvCreateImage(cvSize(tmp->width, tmp->height), IPL_DEPTH_8U, 1);

     // convert to grayscale

     cvCvtColor(tmp, tmp2, CV_RGB2GRAY);
	
	// pgg start
	
	srcMat = (CvMat*) tmp2;
     srcMat = cvGetMat(srcMat, &srcStub, 0, 1);

     int ROWS = srcMat->rows;
     int COLS = srcMat->cols;

     imageMatrixA.Allocate(ROWS, COLS);
     imageMatrixB.Allocate(ROWS, COLS);

	// Mat object

     Mat matimg(tmp2);

	for (int i = 0; i != ROWS; ++i) {
     	for (int j = 0; j != COLS; ++j) {
               unsigned char tmp;
               tmp = ((uchar*) matimg.data + i * matimg.step)[j * matimg.elemSize() + 1];
               imageMatrixA.SetElement(i, j, tmp);
          }
     }

	// test

	cout << " --> " << imageMatrixA(1,1) << endl;
	cout << " --> " << imageMatrixA(1,2) << endl;
	cout << " --> " << imageMatrixA(1,3) << endl;
	cout << " --> " << imageMatrixA(1,4) << endl;

     // apply the wiener filter

     wienerFilterGrayscale2D(imageMatrixA, imageMatrixB, 10, 10);

//	CvScalar scalar(0.1415);
//     cvSet2D(tmp2, 5, 5, scalar);

	IplImage * dvImg;
     CvSize dvSize;

	dvSize.width = COLS;
     dvSize.height = ROWS;

	dvImg = cvCreateImage(dvSize, 8, 1);

     for (int i = 0; i != ROWS; ++i) {
          for (int j = 0; j != COLS; ++j) {
               ((uchar*)(dvImg->imageData + dvImg->widthStep*i))[j] =
                    static_cast<char>(imageMatrixB(i,j));
          }
     }

	cout << " --> " << imageMatrixB(1,1) << endl;
	cout << " --> " << imageMatrixB(1,2) << endl;
	cout << " --> " << imageMatrixB(1,3) << endl;
	cout << " --> " << imageMatrixB(1,4) << endl;

     // save image

     cvSaveImage(result_path.c_str(), dvImg);

     // free up RAM

     cvReleaseImage(&tmp);
     cvReleaseImage(&tmp2);

     // sentineling

     int sentinel;
     cout << " --> Enter an integer to exit: ";
     cin >> sentinel;

     // return

     return 0;
}

// END
