
#ifndef WIENER_FILTER_GRAYSCALE_2D_H
#define WIENER_FILTER_GRAYSCALE_2D_H

#include "../../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// function declaration

void wienerFilterGrayscale2D(const MatrixDense<double> & mat_in,
                             MatrixDense<double> & mat_out,
                             int wiener_filter_rows = 3,
                             int wiener_filter_cols = 3);

#endif // WIENER_FILTER_GRAYSCALE_2D_H

// END
