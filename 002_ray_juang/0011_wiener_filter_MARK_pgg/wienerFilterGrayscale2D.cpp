//=========================//
// wienerFilterGrayscale2D // 
// function implementation //
//=========================//

// C++

#include <cstdio>
#include <iostream>
#include <utility>

using std::cout;
using std::cin;
using std::endl;
using std::pair;

// pgg

#include "wienerFilterGrayscale2D.h"
#include "../../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// function definition

void wienerFilterGrayscale2D(const MatrixDense<double> & imageMatrixA,
                             MatrixDense<double> & destArray,
                             int wiener_k_rows,
                             int wiener_k_cols)
{
	// local variables

     int nRows;
     int nCols;

     MatrixDense<double> kernelAlpha;
     MatrixDense<double> mat_tmp_1;
     MatrixDense<double> mat_tmp_2;
     MatrixDense<double> mat_tmp_3;
     MatrixDense<double> mat_tmp_4;

     unsigned int ROWS;
     unsigned int COLS;
     double noisePower;
     pair<int, int> anchor;

	// start

     nRows = wiener_k_rows;
     nCols = wiener_k_cols;

     // STEP --> 1 --> DONE

     kernelAlpha.Allocate(nRows, nCols);
     kernelAlpha = 1.0/static_cast<double>(nRows*nCols);

     // STEP --> 2 --> DONE

	ROWS = imageMatrixA.GetNumberOfRows();
	COLS = imageMatrixA.GetNumberOfColumns();

     // STEP --> 3 --> DONE

     mat_tmp_1.Allocate(ROWS, COLS);

     anchor.first = nCols/2;
     anchor.second = nRows/2;
     mat_tmp_1.MatrixCorrelate(imageMatrixA, kernelAlpha, anchor);

     // STEP --> 4 --> DONE

     mat_tmp_2.Allocate(ROWS, COLS);
     mat_tmp_2.Times(imageMatrixA, imageMatrixA);
     mat_tmp_2.SetMax(255.0, 255.0);
     mat_tmp_2.SetMin(0.0, 0.0);

     // STEP --> 5 --> DONE

     anchor.first = nCols/2;
     anchor.second = nRows/2;
     mat_tmp_3.Allocate(ROWS, COLS);
     mat_tmp_3.MatrixCorrelate(mat_tmp_2, kernelAlpha, anchor);

     //mat_tmp_3.SetMax(255.0, 255.0);
     //mat_tmp_3.SetMin(0.0, 0.0);

     // STEP --> 6 --> DONE

     mat_tmp_4.Allocate(ROWS, COLS);
     mat_tmp_4.Times(mat_tmp_1, mat_tmp_1);
     mat_tmp_4.SetMax(255.0, 255.0);
     mat_tmp_4.SetMin(0.0, 0.0);

     // STEP --> 7 --> DONE

     mat_tmp_3.Subtract(mat_tmp_3, mat_tmp_4);
     mat_tmp_3.SetMax(255.0, 255.0);
     mat_tmp_3.SetMin(0.0, 0.0);

     // STEP --> 8 --> DONE

     noisePower = mat_tmp_3.Mean();

     // STEP --> 9 --> DONE

     destArray.Allocate(ROWS, COLS);
     destArray.Subtract(imageMatrixA, mat_tmp_1);
     destArray.SetMax(255.0, 255.0);
     destArray.SetMin(0.0, 0.0);

     // STEP --> 10 --> DONE

     mat_tmp_2.SetMax(mat_tmp_3, noisePower);
     mat_tmp_2.SetMax(255.0, 255.0);
     mat_tmp_2.SetMin(0.0, 0.0);

     // STEP --> 11 --> DONE

     mat_tmp_3.Plus(mat_tmp_3, -noisePower);
     mat_tmp_3.SetMax(255.0, 255.0);
     mat_tmp_3.SetMin(0.0, 0.0);

     // STEP --> 12 --> DONE

     mat_tmp_3.SetMax(mat_tmp_3, 0);
     mat_tmp_3.SetMax(255.0, 255.0);
     mat_tmp_3.SetMin(0.0, 0.0);

     // STEP --> 13 --> DONE

     mat_tmp_3.Divide(mat_tmp_3, mat_tmp_2);
     mat_tmp_3.SetMax(255.0, 255.0);
     mat_tmp_3.SetMin(0.0, 0.0);

     // STEP --> 14 --> DONE

     destArray.Times(mat_tmp_3, destArray);
     destArray.SetMax(255.0, 255.0);
     destArray.SetMin(0.0, 0.0);

     // STEP --> 15 --> DONE

     destArray.Plus(destArray, mat_tmp_1);

     // deallocate local help matrices

     mat_tmp_1.Deallocate();
     mat_tmp_2.Deallocate();
     mat_tmp_3.Deallocate();
     mat_tmp_4.Deallocate();

	// return
}

// END
