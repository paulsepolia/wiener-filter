//=========================//
// wienerFilterGrayscale2D // 
// function implementation //
//=========================//

// C++

#include <cstdio>
#include <iostream>
#include <utility>

using std::cout;
using std::cin;
using std::endl;
using std::pair;

// pgg

#include "wienerFilterGrayscale2D.h"
#include "../../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// function definition

void wienerFilterGrayscale2D(const MatrixDense<double> & imageMatrixA,
                             MatrixDense<double> & destArray,
                             unsigned int wiener_k_rows,
                             unsigned int wiener_k_cols)
{
	// local variables and parameters

	const double T55 = 255.0;
	const double ZERO = 0.0;

     MatrixDense<double> kernelAlpha;
     MatrixDense<double> mat_tmp_1;
     MatrixDense<double> mat_tmp_2;
     MatrixDense<double> mat_tmp_3;
     MatrixDense<double> mat_tmp_4;
     unsigned int wkRows;
     unsigned int wkCols;
     unsigned int ROWS;
     unsigned int COLS;
     double noisePower;
     pair<int, int> anchor;

	// step --> 0

     wkRows = wiener_k_rows;
     wkCols = wiener_k_cols;

     // step --> 1

     kernelAlpha.Allocate(wkRows, wkCols);
     kernelAlpha = 1.0/static_cast<double>(wkRows*wkCols);

     // step --> 2

	ROWS = imageMatrixA.GetNumberOfRows();
	COLS = imageMatrixA.GetNumberOfColumns();

     // step --> 3

     mat_tmp_1.Allocate(ROWS, COLS);

     anchor.first = wkCols/2;
     anchor.second = wkRows/2;
     mat_tmp_1.MatrixCorrelate(imageMatrixA, kernelAlpha, anchor);

     // step --> 4

     mat_tmp_2.Allocate(ROWS, COLS);
     mat_tmp_2.Times(imageMatrixA, imageMatrixA);

     // step --> 5

     anchor.first = wkCols/2;
     anchor.second = wkRows/2;
     mat_tmp_3.Allocate(ROWS, COLS);
     mat_tmp_3.MatrixCorrelate(mat_tmp_2, kernelAlpha, anchor);

     // step --> 6

     mat_tmp_4.Allocate(ROWS, COLS);
     mat_tmp_4.Times(mat_tmp_1, mat_tmp_1);

     // step --> 7

     mat_tmp_3.Subtract(mat_tmp_3, mat_tmp_4);

     // step --> 8

     noisePower = mat_tmp_3.Mean();

     // step --> 9

     destArray.Allocate(ROWS, COLS);
     destArray.Subtract(imageMatrixA, mat_tmp_1);

     // step --> 10

     mat_tmp_2.SetMax(mat_tmp_3, noisePower);

     // step --> 11

     mat_tmp_3.Plus(mat_tmp_3, -noisePower);

     // step --> 12

     mat_tmp_3.SetMax(mat_tmp_3, ZERO);

     // step --> 13

     mat_tmp_3.Divide(mat_tmp_3, mat_tmp_2);

     // step --> 14

     destArray.Times(mat_tmp_3, destArray);

     // step --> 15

     destArray.Plus(destArray, mat_tmp_1);
     destArray.SetMax(T55, T55);
     destArray.SetMin(ZERO, ZERO);

	// step --> final

     mat_tmp_1.Deallocate();
     mat_tmp_2.Deallocate();
     mat_tmp_3.Deallocate();
     mat_tmp_4.Deallocate();

	// return
}

// END
