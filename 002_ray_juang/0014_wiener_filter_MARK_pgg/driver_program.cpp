//========//
// Driver //
//========//

// C++

#include <iostream>
#include <string>
#include <iomanip>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// BOOST/GIL

#include <boost/mpl/vector.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/extension/dynamic_image/any_image.hpp>
#include <boost/gil/image_view.hpp>
#include <boost/gil/planar_pixel_reference.hpp>
#include <boost/gil/color_convert.hpp>
#include <boost/gil/typedefs.hpp>
#include <boost/gil/image_view_factory.hpp>
#ifndef BOOST_GIL_NO_IO
#include <boost/gil/extension/io/tiff_io.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include <boost/gil/extension/io/tiff_dynamic_io.hpp>
#include <boost/gil/extension/io/jpeg_dynamic_io.hpp>
#include <boost/gil/extension/io/png_dynamic_io.hpp>
#endif

using namespace boost::gil;

// pgg

#include "wienerFilterGrayscale2D.h"
#include "../../math_pack/MatrixDense.h"

using pgg::MatrixDense;

// the main function

int main()
{
     // input variables

     const string image_name = "gray.jpg";
     const string result_name = "gray_new.jpg";
     const string image_dir = "../../../not_monitored/images";
     const string result_dir = "../../../not_monitored/results";
	const unsigned int wiener_kernel_rows = 10; // >= 2
	const unsigned int wiener_kernel_cols = 10;  // >= 2

     // local parameters and variables

     const string image_path = image_dir + "/" + image_name;
     const string result_path = result_dir + "/" + result_name;
	MatrixDense<double> imageMatrixIn;
	MatrixDense<double> imageMatrixOut;

	// local variables

     gray8_image_t imA;
     unsigned int i;
     unsigned int j;
	vector<unsigned char> vecA;

     // load jpeg image

     jpeg_read_image(image_path, imA);

	// get dimensions of the image

     const unsigned int H = imA.height();
     const unsigned int W = imA.width();

	// allocate images matrices

     imageMatrixIn.Allocate(H, W);
     imageMatrixOut.Allocate(H, W);

     // put the image in the matrix

     for (i = 0; i != H; ++i)
     {
          for (j = 0; j != W; ++j)
          {
               auto iL = i*static_cast<unsigned long int>(W)+j;
               imageMatrixIn.SetElement(i, j, static_cast<double>(view(imA)[iL]));
          }
     }

     // apply the wiener filter

     wienerFilterGrayscale2D(imageMatrixIn, 
					    imageMatrixOut, 
					    wiener_kernel_rows,
                             wiener_kernel_cols);


	// convert double data to unsigned char data

	for (i = 0; i != H; ++i)
     {
          for (j = 0; j != W; ++j)
          {
               vecA.push_back(static_cast<unsigned char>(imageMatrixOut(i,j)));
          }
     }

	// interleaved viw of the image

     auto imOutA = interleaved_view(W, H, (gray8_pixel_t const*)(&vecA[0]), W);

	// save the image

     jpeg_write_view(result_path, imOutA);

	// deallocate matrices

	imageMatrixIn.Deallocate();
	imageMatrixOut.Deallocate();

     // sentineling

     int sentinel;
     cout << " Enter an integer to exit: ";
     cin >> sentinel;

     // return

     return 0;
}

// END
