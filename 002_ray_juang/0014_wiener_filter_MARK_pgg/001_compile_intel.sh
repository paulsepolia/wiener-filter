#!/bin/bash

  # 1. compile

  icpc -O3                         \
       -Wall                       \
       -std=c++11                  \
       wienerFilterGrayscale2D.cpp \
       driver_program.cpp          \
       -I/opt/boost/1560/intel             \
       -L/opt/boost/1560/intel/stage/lib/* \
       -ljpeg                              \
       -ltiff                              \
       -lpng                               \
       -lz                                 \
       -o x_intel
