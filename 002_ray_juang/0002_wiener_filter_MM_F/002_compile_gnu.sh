#!/bin/bash

  # 1. compile

  g++  -O3                   \
       -Wall                 \
       -std=c++0x            \
       cv_wiener_2.cpp       \
       driver_program.cpp    \
       /opt/opencv/249/lib/* \
       -lm                   \
       -fopenmp              \
       -lpthread             \
       -o x_gnu
